=============================
Choropleth Visualization
=============================

| Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2019_10_16
| Date Updated: 2023_01_02
| Tags: Neuroimage visualization, analysis
| Acknowledgements: Andy Dufile, Tom Hicks, Devin Bayly


.. toctree::
   :hidden:
   :maxdepth: 3

Introduction
****************

Choropleths are simple maps that display regional changes as color (e.g., a US map showing states that lean Democrat vs Republican). Here is an example of a brain choropleth.

.. image:: /pictures/choropleth.png

We demonstrated that this simple approach to visualization works well for brains, and, in fact, facilitates analysis:

`Patterson, D. K., Hicks, T., Dufilie, A. S., Grinstein, G., & Plante, E. (2015). Dynamic Data Visualization with Weave and Brain Choropleths. PLoS ONE, 10(9), e0139453–17. http://doi.org/10.1371/journal.pone.0139453 <https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0139453>`_

Obviously brain choropleths are most valueable if you can choose the slice to view and display information about the regions in each slice. The original `dynamic Weave visualizations <https://adufilie.github.io/weave-demo/brain-choropleths/>`_ allowed slice stacking. However, you will need to enable Adobe Flash to view the Weave visualizations, and Weave is no longer available. For this reason, we needed a new implementation.  See Devin Bayly's `Neuro-choropleths Tool <https://devinbayly.github.io/neuro-choro/>`_. This work was supported by the University of Arizona HPC center.  It uses Javascript to display GeoJSON slices and regional information.  Devin's implementation of brain choropleths is fast and easy to use.  You can make notes, compare different views and export your session to load up later.

roixtractor
**************

Even without the brain visualizations, extraction of regional descriptive statistics provides a lot of power for analysis.  You can extract such data by region using a Docker container that includes FSL 5.X and the scripts and atlases for extraction.  See the roixtractor app provided as part of the `scif_fsl tools <https://bitbucket.org/dpat/scif_fsl/src/master/>`_. The `scif_fsl Readme <https://bitbucket.org/dpat/scif_fsl/src/master/README.md>`_ provides more detail. You will need to install `Docker <https://www.docker.com/products/docker-desktop>`_ to run the tools. The scif_fsl git repository includes all the atlas ROIS, if you are curious.

.. Note:: You may have trouble with Docker on older operating systems.

Once Docker is installed, you can obtain an up-to-date copy of the scif_fsl Docker container by pulling it from dockerhub::

  docker pull diannepat/scif_fsl

After downloading the Docker container to your local machine, I suggest getting a copy of the wrapper script `sfw.sh <https://bitbucket.org/dpat/scif_fsl/raw/master/sfw.sh>`_ which facilitates querying and calling the tools in the Docker container. Put this script somewhere in your path. You can begin by running the script with no arguments::

  sfw.sh

Running `sfw.sh <https://bitbucket.org/dpat/scif_fsl/raw/master/sfw.sh>`_ will provide a list of apps in the container and relevant help for using them. roixtractor is one app in the container and is useful for extracting descriptive statistics for each region in a standard MNI 2mm image. You can try roixtractor Stats or Lat functions with the test statistical image `run-01_IC-06_Russian_Unlearnable.nii <https://osf.io/uty3v/>`_ or try the mask function with the `sample_lesion <https://osf.io/hc5tg/>`_.

To run roixtractor you must pass it:

* The function to run: Stats, Lat or Mask.

  * Stats: generates mean and peak values for above threshold voxels in a stats image
  * Lat: generates laterality info for above threshold values in a stats image
  * Mask: generates percent_masked values for each region. It works with a binary mask in standard space.

* An image in 2mm FSL MNI standard space (If it is not in that space, but is MNI, e.g. spm space, it will be backed up to a \*_orig file and then the main image will be converted to the proper space).  If you run Stats or Lat, this should be a statistical image.  If you run Mask, it should be a mask.
* A threshold value:

  * If you are using Stats or Lat, you must specify a statistical threshold (output regions must be equal to or greater than this value).  
  * If you are running Mask, you will typically specify a threshold of ''0''.

* You must specify an available atlas: ARTERIAL1, ARTERIAL2, ARTERIAL1_WM-GM, HO-CB, HCP-MMP1:

  * The three arterial atlases (ARTERIAL1, ARTERIAL2, ARTERIAL1_WM-GM) identify vascular territories, with level 2 providing more detail than level 1.  The ARTERIAL1_WM-GM identifies the intersection of each territory with a GM segmentation from FSL's 1mm MNI space brain. See Liu C-F, Hsu J, Xu X, Kim G, Sheppard SM, Meier EL et al. (2021) Digital 3D Brain MRI Arterial Territories Atlas. BioRxiv for more detail about the level1 and level 2 atlases.
  * HO-CB: The Harvard-Oxford cortical and subcortical atlases combined with a cerebellar atlas.
  * HCP-MMP1: Although it is possible to output these values in HCP-MMP1 space, it is probably not a good idea to use the HCP-MMP1 atlas in volumetric space. It is not likely to be terribly accurate).

The git repository https://bitbucket.org/dpat/scif_fsl/src/master/ includes all the atlas ROIS.

Stats
=======

If you run Stats, roixtractor will generate a CSV file and a NIfTI image containing the regions from the chosen atlas that meet the threshold.

Example run::

    sfw.sh xtract Stats run-01_IC-06_Russian_Unlearnable.nii 1.3 HO-CB

A CSV file is generated: HO-CB_run-01_IC-06_Russian_Unlearnable_1.3_stats.csv, containing a header and values like these:

Region, Mean, Max, SD, LR, Lobe, CogX, CogY, CogZ, RegionLongName, RegionVol_MM3, Image.
BA44_R,1.664311,2.824052,0.597232,R,Fr,19,71,44,Inferior_Frontal_Gyrus_pars_opercularis_R,5240,run-01_IC-06_Russian_Unlearnable_1.5

* Region: the short region name;
* Mean, Max and SD are descriptive statistics for each region.
* LR: identifies the region as left or right (redundant, but useful for sorting)
* Lobe: The lobe in which that region is found
* CogX, CogY and CogZ are the x,y,z center of gravity voxel coordinates for each region
* RegionLongName: A more detailed name for the region if available
* RegionVol_MM3: The volume of the region in cubic mm
* Image: The name of the image you passed to the program: the name of the atlas you used is prepended and the threshold you used is appended to the name. Because image is the last field, you can easily use Excel to split the data on underscores. This splitting is useful if you want to concatenate several CSVs from different runs of the xtractor.

A NIfTI image will also be generated that fills each such region with the mean value:
e.g., HO-CB_run-01_IC-06_Russian_Unlearnable_1.3_stats.nii.gz


Lat
======

The CSV file will contain laterality information based on Wegrzyn, M., Mertens, M., Bien, C. G., Woermann, F. G., & Labudda, K. (2019). Quantifying the Confidence in fMRI-Based Language Lateralisation Through Laterality Index Deconstruction. Frontiers in Neurology, 10, 106915. http://doi.org/10.3389/fneur.2019.00655

The number of suprathreshold voxels in each region is reported for left and right homologs.

Example run:: 

    sfw.sh xtract Lat run-01_IC-06_Russian_Unlearnable.nii 1.3 HO-CB

A csv file is generated: HO-CB_run-01_IC-06_Russian_Unlearnable_1.3_LI.csv, containing a header and values like these:

Region,VoxSuprathresh_L,VoxSuprathresh_R,Strength,Laterality,LI,Lobe,RegionLongName,Image
Acb,0,0,0,0,0,Subcort,Accumbens,run-01_IC-06_Russian_Unlearnable_1.3
Amg,176,200,376,-24,-0.0638298,Subcort,Amygdala,run-01_IC-06_Russian_Unlearnable_1.3

VoxSuprathresh_L is the number of suprathreshold voxels on the left
VoxSuprathresh_R is the number of suprathreshold voxels on the right
Strength=L+R
Laterality=L-R
LI=laterality/strength



Mask
======

The CSV will contain categorical information to characterize the region and two measures: ``MaskVol_in_Region_mm3`` is the cubic mm in the region that are also in the mask. ``MaskVol_in_Region_Percent`` is the percentage of voxels in the region that are also in the mask. Here is a `sample_lesion <https://osf.io/hc5tg/>`_.

Example run:: 

    sfw.sh xtract Mask w1158lesion.nii.gz 0 HO-CB

A CSV file is generated: HO-CB_w1158lesion_mask.csv, containing a header and values like these:

Region,LR,Lobe,RegionLongName,RegionVol_mm3,MaskVol_in_Region_mm3,MaskVol_in_Region_Percent,Image
AnG_L,L,Par,Angular_Gyrus_L,7584,48,.632900,w1158lesion_mask
Cen_Op_L,L,Fr,Central_Opercular_Cortex_L,7872,1176,14.939000,w1158lesion_mask



Resources
************

* `Patterson, D. K., Hicks, T., Dufilie, A. S., Grinstein, G., & Plante, E. (2015). Dynamic Data Visualization with Weave and Brain Choropleths. PLoS ONE, 10(9), e0139453–17. http://doi.org/10.1371/journal.pone.0139453 <https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0139453>`_: This is the original choropleth paper.
* `Neuro Choropleth Tool <https://devinbayly.github.io/neuro-choro/>`_: Devin Bayly, with support from the University of Arizona HPC center, is working on this implementation of a choropleth viewer.
* Sample 3D stats image: `run-01_IC-06_Russian_Unlearnable.nii <https://osf.io/uty3v/>`_.
* `Geo_NIFTI.zip <https://osf.io/judc6/>`_: The regions in NIfTI format for our HO-CB hybrid atlas. This includes ROIS.zip which is individual binary masks for each region and HO-CB_all.nii.gz which combines all the label regions and can be downloaded and used for display of the regions in the hybrid atlas.
* `HO-CB GeoJSON files <https://osf.io/2xzvb/>`_. Only for the curious
* `HCP-MMP1 GeoJSON files <https://osf.io/ukaeg/>`_. Only for the curious
* `CreateGeoJSON.zip <https://osf.io/gbj43/>`_: Bash and Matlab scripts for creating your own GeoJSON files from NIfTI.
* `Tableau_Brains.zip <https://osf.io/ha236/>`_ and an accompanying workbook: `Tableau_Workshop.zip <https://osf.io/3nysg/>`_. A June 2017 implementation of the slices for Tableau.  Note, there is no option to scroll through GeoJSON slices in Tableau, you have to pick a static slice for display.
* `WeaveTutorial.zip <https://osf.io/x4pmw/>`_: CSV data suitable for display on the GeoJSON slices (and a complete Weave tutorial) (2015).  Deprecated.


License
************

`The NIfTI format atlas: HO_Cb_all.nii.gz, the 0.5 mm standard space MNI brain: MNI152_T1_0.5mm.nii.gz and the ROIs for all regions <https://osf.io/judc6/>`_
are protected by `the FSL license <https://fsl.fmrib.ox.ac.uk/fsldownloads_registration>`_
Unless explicitly stated, any materials not directly protected by the FSL license fall under the `Creative Commons Attribution-NonCommerical 4.0 license <http://creativecommons.org/licenses/by-nc/4.0/legalcode>`_

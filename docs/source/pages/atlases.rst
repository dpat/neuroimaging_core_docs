============
Atlases
============

| Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2019_10_31
| Date Updated: 2023_07_16
| Tags: Neuroimaging software, Atlases, FSL
| OS: Mostly UNIX (e.g., Mac or Linux)

* Links to the atlas resources can be found on the bookmarks page :ref:`atlas_bookmarks`.
* FSL comes with a range of atlases prepared for viewing in FSLeyes. Learn more on the `Atlases <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Atlases>`_ page.
* FSL atlases can be created by following these  `atlas reference rules <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Atlases-Reference>`_
* Atlases with discontinuous labels, like ARTERIAL1_WM-GM and HCP-MMP1 are best viewed with the random (big) LUT in FSLeyes.  Other LUTS may fail to show all the labels.

How to Download and Install Additional FSL Atlases
*********************************************************

Below I provide several additional atlases to display in FSLeyes. To use these atlases, download the zip file by clicking on the atlas title.  Place the unzipped folder and xml file(s) in the FSL atlases directory (e.g., /usr/local/fsl/data/atlases). The next time you start fsleyes, the atlas should be available in the atlas panel.

----

`Arterial Territory <https://osf.io/e3r7b/>`_
***************************************************

This zip file contains three arterial atlases (ARTERIAL1, ARTERIAL2, ARTERIAL1_WM-GM) corresponding to the atlases defined in Liu C-F, Hsu J, Xu X, Kim G, Sheppard SM,Meier EL et al. (2021) Digital 3D Brain MRI Arterial Territories Atlas. BioRxiv.

The atlases identify vascular territories, with level 2 providing more detail than level 1.  The additional ARTERIAL1_WM-GM identifies the intersection of each territory with a GM segmentation from FSL's 1mm MNI space brain: Each GM region of a corresponding arterial territory has the same label as the original territory + 100 (e.g., label 16 now corresponds to the WM for the territory and 116 corresponds to the GM for that territory). The 1mm label images were downloaded from NITRC: https://www.nitrc.org/projects/arterialatlas/


`BIP <https://osf.io/cbk3u/>`_
************************************

The BIP language atlas is based on: Patterson, D. K., Van Petten, C., Beeson, P., Rapcsak, S. Z., & Plante, E. (2014). Bidirectional iterative parcellation of diffusion weighted imaging data: separating cortical regions connected by the arcuate fasciculus and extreme capsule. NeuroImage, 102 Pt 2, 704–716. `http://doi.org/10.1016/j.neuroimage.2014.08.032 <https://www.sciencedirect.com/science/article/pii/S105381191400706X?via%3Dihub>`_

The regions and tracts are described in the `Readme.txt <https://bitbucket.org/dpat/bipbids/src/master/REF/IMAGES/Readme_tracts.txt>`_

The software for running BIP is available as a containerized BIDS app.  See :ref:`bip container <bipcontainer>` for a description and the `bipbids bitbucket site <https://bitbucket.org/dpat/bipbids/src/master/>`_ for the code.  Note that the `Singularity recipe <https://bitbucket.org/dpat/bipbids/raw/master/Singularity_bip>`_ contains code to use NVIDIA GPUs on out HPC.  The Docker container, which can be pulled from dockerhub ``docker pull diannepat/bip`` does not contain the GPU code.


`HCP-MMP1 and HCP-MMP1_cortices <https://osf.io/azup8/>`_
****************************************************************

The HCP-MMP1 atlas is based on the asymmetrical version of the MNI projection of the HCP-MMP1 (MMP_in_MNI_corr.nii.gz) available on `figshare <https://figshare.com/articles/HCP-MMP1_0_projected_on_MNI2009a_GM_volumetric_in_NIfTI_format/3501911>`_ and `neurovault <https://neurovault.org/collections/1549/>`_.

In each hemisphere, Glasser divides 180 "areas" into 22 separate "regions". Here I refer to the 180 areas as **regions** to be consistent with other atlases where "region" generally refers to the smallest partition of interest.  I call the 22 larger partitions **cortices**.

In this HCP-MMP1 atlas the 180 regions are numbered 1-180.  On the right the regions are numbered 201-380 so that 201 is the right homologue of 1; 262 is the right homologue of 62, etc.) Note that MRtrix3 renumbers the values on the right to go from 181 to 360 to avoid the discontinuity (i.e., unused values between 181 and 199). The original atlas uses 1-180 on the right and 201-380 on the left. MRtrix and some other verisons of the atlas (like this one) swap the left and right labels (hence 1-180 on the left, 201-380 on the right). 

Each of the 180 regions occupies one of 22 cortices which are displayed in a separate atlas: HCP-MMP1_cortices. These are numbered 1-22 on the left and 101-122 on the right, in keeping with the original strategy.

Detailed information about the regions and cortices are available in the Glasser 2016 Supplemental file: "Supplementary Neuroanatomical Results For A Multi-modal Parcellation of Human Cerebral Cortex".  I have made the the following helpful files available:

#. The final table of 180 regions from that supplemental file available as an Excel sheet: `Glasser_2016_Table.xlsx <https://bitbucket.org/dpat/tools/src/master/REF/ATLASES/Glasser_2016_Table.xlsx>`_
#. `HCP-MMP1_UniqueRegionList.csv <https://bitbucket.org/dpat/tools/raw/master/REF/ATLASES/HCP-MMP1_UniqueRegionList.csv>`_ providing information from the final table in addition to a center of gravity in voxel coordinates and a volume in cubic mm for each of the 360 regions (180 in each hemisphere).
#. `A text file <https://bitbucket.org/dpat/tools/raw/master/REF/ATLASES/HCP-MMP1_cortices.txt>`_ naming the 22 cortices and how they are grouped as per descriptions in the supplemental material.
#. `HCP-MMP1_cortex_UniqueRegionList.csv <https://bitbucket.org/dpat/tools/raw/master/REF/ATLASES/HCP-MMP1_cortex_UniqueRegionList.csv>`_ providing the center of gravity in voxel coordinates and the volume in cubic mm for each of the 44 cortices (22 in each hemisphere).

This is the accompanying paper:
Glasser, M. F., Coalson, T. S., Robinson, E. C., Hacker, C. D., Harwell, J., Yacoub, E., et al. (2016). A multi-modal parcellation of human cerebral cortex. Nature, 1–11. `http://doi.org/10.1038/nature18933 <https://www.nature.com/articles/nature18933>`_

`GreciusFunc <https://osf.io/x76r5/>`_
********************************************

This is a 2mm probabalistic atlas with 13 functional regions defined. It includes both cortex and cerebellum. The data for each roi were downloaded from http://findlab.stanford.edu/functional_ROIs.html

This is the accompanying paper:

Shirer, W. R., Ryali, S., Rykhlevskaia, E., Menon, V., & Greicius, M. D. (2012). Decoding Subject-Driven Cognitive States with Whole-Brain Connectivity Patterns. Cerebral Cortex. http://doi.org/10.1093/cercor/bhr099


`Schaefer atlases <https://osf.io/7j4zu/>`_
***************************************************

Schaefer atlases define functional regions and are available from their `git repository <https://github.com/ThomasYeoLab/CBIG/tree/master/stable_projects/brain_parcellation/Schaefer2018_LocalGlobal>`_.

See Schaefer A, Kong R, Gordon EM, Laumann TO, Zuo XN, Holmes AJ, Eickhoff SB, Yeo BTT. Local-Global parcellation of the human cerebral cortex from intrinsic functional connectivity MRI. Cerebral Cortex, 29:3095-3114, 2018.

The two atlases made available here are converted into the native FSL atlas format.


`YeoBuckner7 and YeoBuckner17 <https://osf.io/5ayw4/>`_
*************************************************************

These are 2mm probabalistic atlases converted from Freesurfer space to FSL standard space with the cortex and cerebellum combined.  The data comes from http://surfer.nmr.mgh.harvard.edu/fswiki/CorticalParcellation_Yeo2011 and http://www.freesurfer.net/fswiki/CerebellumParcellation_Buckner2011
The two atlases are:

#. YeoBuckner7: The 7 functional networks
#. YeoBuckner17: Finer divisions of the 7 networks into a total of 17 networks (which are not always proper subsets of the first 7)

These are the accompanying papers:

Yeo, B. T. T., Krienen, F. M., Sepulcre, J., Sabuncu, M. R., Lashkari, D., Hollinshead, M., et al. (2011). The organization of the human cerebral cortex estimated by intrinsic functional connectivity. Journal of Neurophysiology, 106(3), 1125–1165. http://doi.org/10.1152/jn.00338.2011

Buckner, R. L., Krienen, F. M., Castellanos, A., Diaz, J. C., & Yeo, B. T. T. (2011). The organization of the human cerebellum estimated by intrinsic functional connectivity. Journal of Neurophysiology, 106(5), 2322–2345. http://doi.org/10.1152/jn.00339.2011

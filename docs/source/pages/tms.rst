=====================
TMS
=====================

| Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2019_06_23
| Date Updated: 2019_09_18
| Tags: hardware
| Acknowledgements: Aneta Kielar, Yu-chin Chen, Mark Sundman, Sara Mohr, Mike Shen

.. toctree::
   :hidden:
   :maxdepth: 3

.. |brainseg| image:: /pictures/TMS_BrainSegmentation.png
   :alt: TMS-Navigator interface for brain segmentation: the segmentation is incomplete and will need to have more cerebellum added in.

.. |checkcamera| image:: /pictures/TMS_CheckCamera.png
   :alt: TMS-Navigator camera check interface: all reflector balls have been located successfully.
   :width: 56%

.. |coil2_stimulator| image:: /pictures/TMS_Coil2-Enabled-StimulatorPanel.png
   :alt:

.. |coilcalibration| image:: /pictures/TMS_CoilCalibration.png
   :alt: The small coil and calibration plate (left).  The TMS-Navigator coil calibration interface, with green arrows showing that the coil has been detected correctly.

.. |control_area| image:: /pictures/TMS_control_area-panels.png
   :alt:
   :width: 52%

.. |dicom1| image:: /pictures/TMS_Dicom-load1.png
   :alt: TMS-Navigator interface for loading dicom images: choose DICOM file format
   :width: 55%

.. |dir_struct| image:: /pictures/TMS_dir_structure.png
   :alt: The preferred directory structure for TMS navigator

.. |electrodeplacement| image:: /pictures/TMS_Electrode-Placement.png
   :alt: MEP electrodes placed on hand: red and white elctrode placement (left); black ground electrode on wrist (right)

.. |entrytarget_panel2| image:: /pictures/TMS_Entry-Target-ControlPanel2.png
   :alt: TMS-Navigator interface for viewing and editing list of entry-target locations for stimulation: Hand knob selected.
   :width: 49%

.. |handmuscles| image:: /pictures/TMS_hand_muscles.png
   :alt: Muscles commonly used for establishing the motor threshold for TMS. APB (Abductor Pollicis Brevis).  FDI (First Dorsal Interosseous) and the ADM (Abductor Digiti Minimi). 

.. |handknob| image:: /pictures/TMS_HandKnob.png
   :alt: Three views of the hand knob gyrus in the brain (axial, sagittal, and midline sagittal)

.. |headsurface| image:: /pictures/TMS_HeadSurface.png
   :alt: TMS-Navigator interface for identifying the surface of the head: it looks successful in this case (there are no major holes in the mask)

.. |chinrest| image:: /pictures/TMS_hardware_chinrest.png
   :alt:

.. |refmarker| image:: /pictures/TMS_hardware-ReferenceMarkers.png
   :alt: reflector balls on: mannequin head (left); the pointer: which is very sharp and must be used with care (upper right); a coil and calibration plate (lower right)

.. |refmarker2| image:: /pictures/TMS_hardware-ReferenceMarkers2.png
   :alt: reflector ball case stick on reference tracker, headband reference tracker and pointer

.. |camera_table| image:: /pictures/TMS_hardware_Camera_Table.png
   :alt: On the left is the MagPro Cart.  The large Cool B65 coil and small C-B60 coil are labeled.  The cooling unit is on the bottom of the cart is marked CU. On the right is the Spectra P7 camera above the monitors for the Localite computer and the MEP computer.  Spike7 and Pest icons are displayed on the MEP computer monitor.

.. |coil_install| image:: /pictures/TMS_hardware_coil_install_animation.gif
   :alt:

.. |coil_remove| image:: /pictures/TMS_hardware_coil_removal_animation.gif
   :alt:

.. |footpedals| image:: /pictures/TMS_hardware_footpedals.png
   :alt:
   :width: 26%

.. |magpro_interface| image:: /pictures/TMS_hardware_MagPro-Interface.png
   :alt:

.. |magpro_screen1| image:: /pictures/TMS_hardware_MagPro_iTBS_timing.png


.. |magpro_power| image:: /pictures/TMS_hardware_MagProPower.png
   :alt:

.. |magpro_screen_interface| image:: /pictures/TMS_hardware_MagPro_screen_interface.png
   :alt:

.. |mep| image:: /pictures/TMS_hardware_MEP.png
   :alt:
   :width: 39%

.. |icon_brainseg| image:: /pictures/TMS_icon_BrainSegmentation.png
   :width: 7%
   :alt:

.. |icon_brainseg_small| image:: /pictures/TMS_icon_BrainSegmentation.png
   :width: 4%
   :alt:

.. |icon_bc| image:: /pictures/TMS_icon_Brightness-Contrast.png
   :width: 7%

.. |icon_checkcamera| image:: /pictures/TMS_icon_CheckCamera.png
   :width: 7%
   :alt:

.. |icon_checkcamera_small| image:: /pictures/TMS_icon_CheckCamera.png
   :width: 4%
   :alt:

.. |icon_edit| image:: /pictures/TMS_icon_Edit.png
   :width: 7%
   :alt:

.. |icon_entrytarget| image:: /pictures/TMS_icon_Entry-Target.png
   :width: 7%
   :alt:

.. |icon_eyeclosed| image:: /pictures/TMS_icon_Eye-Closed.png
   :width: 7%
   :alt:

.. |icon_instrument| image:: /pictures/TMS_icon_Instrument.png
   :width: 7%
   :alt:

.. |icon_navigation| image:: /pictures/TMS_icon_Navigation.png
   :width: 7%
   :alt:

.. |icon_navigationmode| image:: /pictures/TMS_icon_NavigationMode.png
   :width: 7%
   :alt:

.. |icon_noref| image:: /pictures/TMS_icon_NoReferenceDetected.png
   :width: 5%
   :alt:

.. |icon_patient_reg| image:: /pictures/TMS_icon_PatientRegistration.png
   :width: 7%
   :alt:

.. |icon_patient_reg_small| image:: /pictures/TMS_icon_PatientRegistration.png
   :width: 4%
   :alt:

.. |icon_peelbrain| image:: /pictures/TMS_icon_PeelBrain.png
   :width: 7%
   :alt:

.. |icon_pest| image:: /pictures/TMS_icon_Pest.png
   :width: 7%
   :alt:

.. |icon_planning| image:: /pictures/TMS_icon_Planning.png
   :width: 7%
   :alt:

.. |icon_planningmode| image:: /pictures/TMS_icon_PlanningMode.png
   :width: 7%
   :alt:

.. |icon_setupsession| image:: /pictures/TMS_icon_Setup-Session.png
   :width: 7%
   :alt:

.. |icon_setupsession_small| image:: /pictures/TMS_icon_Setup-Session.png
   :width: 4%
   :alt:

.. |icon_spikeslow| image:: /pictures/TMS_icon_spike2-slow.png
   :width: 7%
   :alt:

.. |icon_spikespeed| image:: /pictures/TMS_icon_spike2-speed.png
   :width: 7%
   :alt:

.. |icon_spike| image:: /pictures/TMS_icon_spike2.png
   :width: 7%
   :alt:

.. |icon_stimulation| image:: /pictures/TMS_icon_Stimulation.png
   :width: 7%
   :alt:

.. |icon_stimulator| image:: /pictures/TMS_icon_Stimulator.png
   :width: 7%
   :alt:

.. |icon_tmsnavigator| image:: /pictures/TMS_icon_TMS-Navigator.png
   :width: 7%
   :alt:

.. |inputpatient| image:: /pictures/TMS_Input-Patient.png
   :alt:

.. |instrument_controlpanel| image:: /pictures/TMS_Instrument-ControlPanel.png
   :alt:
   :width: 50%

.. |instrumentmarkers_controlpanel| image:: /pictures/TMS_InstrumentMarkers-ControlPanel.png
   :alt:

.. |navigator_interface| image:: /pictures/TMS_Interface.png
   :alt: The TMS-Navigator graphical interface with 5 important areas identified

.. |mniplan_targetdef| image:: /pictures/TMS_MNIPlanning-DefineTarget.png
   :alt:

.. |mniplan_listdef| image:: /pictures/TMS_MNIPlanning-NewList.png
   :alt:

.. |mriformat| image:: /pictures/TMS_MRI-Format.png
   :alt:

.. |navigation| image:: /pictures/TMS_Navigation.png
   :alt: Navigation control panel on left, the three navigation views on the right. From top to bottom: 3D scene, entry/target view and instrument view

.. |navigation_3dscenes| image:: /pictures/TMS_Navigation-3DScenes.png
   :alt:

.. |nifti_load| image:: /pictures/TMS_Nifti_load.png
   :alt:

.. |patient_reg1| image:: /pictures/TMS_Patient-Registration-Active.png
   :alt: Active registration in the TMS-Naviagator Patient Registration Interface

.. |patient_reg2| image:: /pictures/TMS_Patient-Registration.png
   :alt: TMS-Naviagator Patient Registration Interface

.. |patient_reg3| image:: /pictures/TMS_Patient-Registration-RMS.png
   :alt: TMS-Naviagator Patient Registration Interface showing the RMS of the landmark placement. A small RMS (less than 4) is acceptable.

.. |patient_reg4| image:: /pictures/TMS_Patient-Registration-SurfaceRegistration.png
   :alt: TMS-Naviagator Patient Registration Interface showing surface registration and RMS results. A small RMS (less than 4) is acceptable.

.. |pestresults| image:: /pictures/TMS_Pest-results.png
   :alt:

.. |pestsetup| image:: /pictures/TMS_Pest-Setup-New.png
   :alt:

.. |spike2_interface| image:: /pictures/TMS_Spike2_Interface.png
   :alt:

.. |spike2_interface2| image:: /pictures/TMS_Spike2_Interface2.png
   :alt:

.. |spike2_newdoc| image:: /pictures/TMS_Spike2_NewDoc.png
   :alt:

.. |spike2_resourcefile| image:: /pictures/TMS_Spike2_ResourceFile.png
   :alt:

.. |spike2_y| image:: /pictures/TMS_Spike2_y-axis.png
   :alt:

.. |spike2_quit| image:: /pictures/TMS_Spike2-quit.png
   :alt:

.. |spike2_startsample| image:: /pictures/TMS_Spike2_StartSampling.png
   :alt:

.. |stimulation_controlpanel| image:: /pictures/TMS_Stimulation-ControlPanel.png
   :alt:

.. |stimulation_controlpanel_reponses| image:: /pictures/TMS_Stimulation-ControlPanel-Responses.png
   :alt:

.. |stimulator_controlpanel| image:: /pictures/TMS_Stimulator-ControlPanel.png
   :alt:

.. |stimulator_enable| image:: /pictures/TMS_Stimulator-enable.png
   :alt:
   :width: 20%

.. |talairach| image:: /pictures/TMS_Talairach.png
   :alt:

.. |talairach-def| image:: /pictures/TMS_Talairach-def.png
    :alt:

.. |talairach-def2| image:: /pictures/TMS_Talairach-def2.png
    :alt:

.. |toolbar| image:: /pictures/TMS_toolbar.png
   :alt:

.. |toolbar-lower| image:: /pictures/TMS_toolbar-lower.png
   :alt:
   :width: 14%

.. |toolbar-upper| image:: /pictures/TMS_toolbar-upper.png
   :alt:
   :width: 23%

Introduction
****************

This page describes the TMS system at the University of Arizona.  This equipment belongs to the Chou lab. To request access, contact Ying-hui Chou (yinghuichou at email.arizona.edu) to discuss your proposal. The equipment resides in a locked room in the basement of the `BSRL <http://imagingcores.arizona.edu/brl>`_, down the hall from the MRI scanner.

* If you find inaccuracies (or typos or broken links), please let me know: dkp at email.arizona.edu. The descriptions provided below are based on extensive and excellent lab manuals prepared by Yu-chin Chen and Aneta Kielar. This page should provide you with a good sense of how you might proceed in a typical study, but it is not meant to be exhaustive. There is, for example, a 200+ page manual on the TMS-Navigator software (sorry, this is proprietary and cannot be posted).

* It might be helpful to have some kind of `TMS Checklist <https://osf.io/8bcye/>`_. The linked one is in Word format so you can edit it to add or remove the details you need.

A short (3.5 minute) `TMS Experience movie <https://osf.io/276pd/>`_ is available to show potential participants what it is like to get set up for TMS.

**Want PDFS?** Read the Docs has the ability to output documentation in other formats: pdf and epub.  On the bottom left it says ``Read the Docs v:latest``. Under ``Downloads``, choose one of the ``latest`` options (e.g. PDF).  The PDFs have much nicer formatting than if you simply "save as" pdf in your browser. The only downside is that the pdf includes everything on the website, so you'll want to use Preview or Adobe Acrobat or something similar to extract the portion of the documentation you want. Of course, you could just print the range of pages you want as hard copies.


.. _tmshardware:

Hardware
*************

To use the TMS (Transcranial Magnetic Stimulation) for research, we rely on three separate hardware systems.  Here they are:

* MagPro
* Localite Computer and Camera
* :abbr:`MEP (Motor Evoked Potential)`

MagPro
========

.. figure:: /pictures/TMS_hardware_Camera_Table.png

  **Left**: The MagPro implements desired pulse sequences. The small C-B60 coil ``2`` administers single pulses: useful for demonstration and for identifying the resting motor threshold. The large Cool-B65 coil ``1`` administers pulse trains. Administering pulse trains requires the liquid cooling unit ``3`` and associated isolation transformer on the bottom of the MagPro cart. **Right**: The Localite computer runs the TMS Navigator software and the associated Polaris Spectra P7 camera ``4`` tracks objects using infrared and specialized reflective reference balls. The MEP computer provides Spike 2 and Pest software. The MEP monitor power is controlled by a joystick on the back middle bottom of the monitor (**arrow**).

.. figure:: /pictures/TMS_hardware_MagPro-Interface.png

  MagPro Interface: 1) Amplitude Wheel; 2) Options Wheel; 3) Enable/Disable Button; 4) Soft Keys. The function of each soft key is shown in the display just above the key. The coil plugs in on the left.

.. figure:: /pictures/TMS_hardware_MagPro_screen_interface.png

  MagPro Screen: 1=status area; 2=information area; 3=selection area; 4=soft key area.  This figure displays the default **Main** screen. ``1``:  The amplitude wheel is one way to control the amplitude of stimulation (cyan field on the upper left). The Enable/Disable button controls the **Status** of the coil: |stimulator_enable|. The status area consists of fixed state fields, that do not change when you switch between **Main** and **Timing**. ``3``: The pulse sequence name is displayed in the **Setup** field (cyan). The Options Wheel scrolls through the available pulse sequences.  After finding the desired pulse sequence, press the **Recall** soft key. ``4``: The leftmost soft key switches to the timing menu if you wish to see details of the pulse sequence. Once in the timing menu, the leftmost soft key switches to say **Main** so you can switch back to the Main display when finished. You can see an example of the Timing display in the :ref:`TBS section <TBS>`

|coil_install|    |coil_remove|

**Left**: The C-B60 coil being attached to the MagPro; **Right** the same coil being detached from the MagPro.


-----

.. figure:: /pictures/TMS_hardware-ReferenceMarkers.png

  Sets of three silver reference balls are attached to the participant's head **left**, the pointer **upper right**; the C-B60 coil and the calibration plate **lower right**. The three ball tracker on the participant’s head is called the reference tracker. Try not to touch or nick the reference balls to keep them clean and undamaged. The pointer is very sharp: guide the pointer with your finger, covering the tip to prevent slipping/unwanted contact.


.. figure:: /pictures/TMS_hardware-ReferenceMarkers2.png

  The reflector balls are stored in a clean safe padded case: The stick-on reference tracker ``1``; the pointer ``2``; and the headband reference tracker ``3``.

.. figure:: /pictures/TMS_hardware_MagProPower.png

  **Left**: Turn on the MagPro (power switch is on the back, red rectangle) and allow it to boot up completely **before** turning on (**Right**) the Localite computer (under the table; power button in red rectangle) to which it is connected. This sequence ensures the Localite computer can detect the MagPro. In addition, before using the large Cool-B65 coil, turn on the cooling unit (else the coil will remain disabled). The cooling unit on the bottom of the MagPro cart, and its power toggle is on the front.


----

MEP
=====

.. figure:: /pictures/TMS_hardware_MEP.png

  **Left**: From top to bottom: The MEP (Muscle Evoked Potentials) system consists of an amplifier (white), data acquisition unit (black), and computer (black Dell) on a cart. **Right**: Power on white amplifier (switch on lower right) and black data acquisition unit (toggle on back of device). Generally, the MEP computer is already on, but power it on if it isn’t. The monitor, keyboard and mouse for the MEP are on the table to the right of the Localite monitor.

-----

.. _tmsnavigator:

|icon_tmsnavigator| `TMS-Navigator <https://osf.io/x7hec/>`_ Interface
*****************************************************************************

.. figure:: /pictures/TMS_Interface.png

  Five important areas: 1. Control area; 2. Display; 3. Toolbar; 4. Menu; 5. Status bar

----

.. figure:: /pictures/TMS_control_area-panels.png

  \1. The **Control Area** is on the right of the display and divided into an upper **Control Modes** area and a **Lower Control Area**. Start with default **Planning Mode** (selected here). We will switch to **Navigation Mode** (by selecting the associated radio button) later. Clicking an icon in the **Lower Control Area** opens the corresponding **control panel** to the left of the Control Area. In this figure, we see the Planning control panel, and below that the title of the Entry/Target control panel. To the right of each control panel title are two arrows ``↑`` and ``↓`` to move the control panel up or down and a ``-`` to close the panel.

----

| 2. **Display**: The view above (in the TMS Navigator Interface figure) displays four equal sized panels. To change the layout, choose *View*  ➜  *Layout* in the menu.  For example, you can maximize the size of panel 4 to make it easier to see the 3D representation.  If the layout options are not available (e.g., while navigating) you can still zoom in the 3D panel (hold down the mouse wheel while moving the mouse).

----

.. figure:: /pictures/TMS_toolbar.png

  \3. The **Toolbar** is on the left of the display area and divided into two sections illustrated above. **Left**: The upper section of the toolbar contains frequently used functions. We will discuss four of these functions in more detail (blue rectangles): |icon_setupsession_small| setup session, |icon_patient_reg_small| patient registration, |icon_brainseg_small| brain segmentation and |icon_checkcamera_small| camera check.  **Right**: The lower section of the toolbar contains four icons that indicate whether devices are being detected by the camera (Red=No; Green=Yes): Pointer; Coil1; Coil2; Reference (on the participant’s forehead).

----

4. The menu provides access to the Talairach definitions, MNI Planning, and lots of other things.

-----

.. figure:: /pictures/TMS_hardware_footpedals.png

  \5. The **Status bar** along the bottom of the interface displays tiny yellow, blue and green icons that describe the footpedal functions for the active mode. These same yellow, blue and green icons appear in various control panels where they implement the same functions as the footpedals.

----

.. _mniplanning:

Once per Project: Folders and MNI Planning
*******************************************

.. Note:: To create and edit MNI planning scenarios, a session does not have to be started or loaded, and the tracking system and instruments are not required.

* Create a folder on each computer (Localite and MEP) to store participant data for your lab.
* On the Localite computer, use **MNI Planning** to create one or more lists of stimulation targets in standard space:

  * Menu: File ➜ MNI Planning ➜ ``+`` ➜ |icon_edit|
* Subsequently you can load these saved targets (You can always edit this later if necessary).

.. figure:: /pictures/TMS_MNIPlanning-NewList.png

    Here we are creating a new list: Aneta_lab. As soon as we click the **edit** button we can populate the list with our targets specified in standard space.

.. figure:: /pictures/TMS_MNIPlanning-DefineTarget.png

  MNI Planning can save you a lot of time, especially if you need to identify several stimulation targets.
  **Left**: After selecting the edit button for the MNI Planning list, enter a list of targets using the ``+``.  Here we define the left hand knob.  You will almost always want either the left or right hand knob defined in order to test for stimulation threshold. Subsequently, edit (pencil in red circle) each target.
  **Right**: Ensure you are using the desired coordinate system (green rectangle) **before** you set the target coordinates and color (blue rectangle). Press **Save** when you finish defining the targets in your list.

-----

Flowcharts
*************

Now you are somewhat familiar with the :ref:`TMS hardware <tmshardware>` and :ref:`the TMS Navigator software <tmsnavigator>`. Hopefully, you have set up :ref:`MNI Planning <mniplanning>`. Now you can proceed to actually set up and run a participant.  In the flowcharts below, clicking a node with a blue border will take you to the relevant section. Hardware=pink. Physical activities=cyan. Localite software=grey. MEP software=green. If order does not matter, a dotted line is used as a connector.

Before Participant Arrives
====================================

.. mermaid::

  graph TD
    subgraph MEP
    style M1 fill:#f9f, stroke:#35F,stroke-width:4px
    style M1a fill:#f9f, stroke:#35F,stroke-width:4px
    style M2 fill:#b3ffb3, stroke:#35F,stroke-width:4px
    style M3 fill:#b3ffb3, stroke:#35F,stroke-width:4px
    style M4 fill:#b3ffb3, stroke:#35F,stroke-width:4px
    style M5 fill:#b3ffb3, stroke:#35F,stroke-width:4px
    click M1 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#mep"
    click M2 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-spike-spike2-setup-mep-computer"
    click M3 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#new-data-document"
    click M4 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#apply-resource-file"
    click M5 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#tms-emg-configuration"
    M1("Black DA:ON")-->M1a("Amp:ON")
    M1a-->M2("Start Spike2")
    M2-->M3("New Data Document")
	  M3-->M4("Apply Resource File")
	  M4-->M5("Configure TMS-EMG")
    end
    subgraph TMS
    style T1 fill:#f9f, stroke:#35F,stroke-width:4px
    style T2 fill:#f9f
    style T3 stroke:#35F,stroke-width:4px
    style T4 stroke:#35F,stroke-width:4px
    style T5 stroke:#35F,stroke-width:4px
    style T6 stroke:#35F,stroke-width:4px
    style T7 stroke:#35F,stroke-width:4px
    style T8 stroke:#35F,stroke-width:4px
    style T9 stroke:#35F,stroke-width:4px
    style T10 stroke:#35F,stroke-width:4px
    style T11 stroke:#35F,stroke-width:4px
    style T12 stroke:#35F,stroke-width:4px
    style T13 stroke:#35F,stroke-width:4px
    style T14 fill:#cff, stroke:#35F,stroke-width:4px
    click T1 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#magpro"
    click T3 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-tmsnavigator-tms-navigator-interface"
    click T4 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-setupsession-set-up-session-localite-computer-toolbar"
    click T5 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-setupsession-set-up-session-localite-computer-toolbar"
    click T6 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#load-anatomical-image"
    click T7 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#surface-threshold"
    click T8 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-patient-reg-patient-registration-landmark-setup-toolbar"
    click T9 " https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-brainseg-brain-segmentation-toolbar"
    click T10 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#define-talairach-menu"
    click T11 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#load-mni-planning-targets"
    click T12 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#the-hand-knob"
    click T13 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#rotation-and-entry-for-each-target"
    click T14 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#set-up-participant"
    T1("MagPro: ON")-->T2("Localite computer: ON")
    T2-->T3("Start TMS Navigator")
    T3-->T4("Load Planning Bookmark")
    T3-->T5("Create Subj & Anat Dir")
	  T5-->T6("Load Anatomicals")
    T6-->T7("Surface Threshold")
    T7-->T8("Patient Reg: Landmark Setup")
    T8-.-T9("Brain Segmentation")
    T9-->T10("Define Talairach")
    T10-->T11("Load MNI Planning")
	  T11-->T12(Handknob)
    T11-->T13("Target: Rotation & Entry")
    T13-->T14("Set up Participant")
    T4-->T14
    end

After Participant Arrives
=================================

.. mermaid::

  graph TD
    style P0 fill:#cff, stroke:#35F,stroke-width:4px
    style P1 fill:#cff
    style P1a fill:#cff
    style P2 fill:#cff, stroke:#35F,stroke-width:4px
    style P3 fill:#cff
    style P4 fill:#cff, stroke:#35F,stroke-width:4px
    style P5 fill:#cff, stroke:#35F,stroke-width:4px
    style P6 fill:#cff, stroke:#35F,stroke-width:4px
    style P7 fill:#f9f, stroke:#35F,stroke-width:4px
    style P8 stroke:#35F, stroke-width:4px
    style P9 stroke:#35F, stroke-width:4px
    style P10 stroke:#35F, stroke-width:4px
    style P11 stroke:#35F, stroke-width:4px
    style P12 stroke:#35F, stroke-width:4px
    style P13 stroke:#35F, stroke-width:4px
    style P14 fill:#f9f, stroke:#35F,stroke-width:4px
    style P15 fill:#f9f
    style P16 fill:#b3ffb3, stroke:#35F,stroke-width:4px
    style P17 fill:#b3ffb3, stroke:#35F,stroke-width:4px
    style P17a fill:#b3ffb3, stroke:#35F,stroke-width:4px
    style P18 fill:#b3ffb3, stroke:#35F,stroke-width:4px
    style P19 fill:#b3ffb3, stroke:#35F,stroke-width:4px
    style P20 fill:#b3ffb3
    style P21 fill:#b3ffb3, stroke:#35F,stroke-width:4px
    style P22 fill:#b3ffb3
    style P23 fill:#b3ffb3, stroke:#35F,stroke-width:4px
    style P24 fill:#b3ffb3
    style P25 fill:#f9f
    style P26 fill:#cff, stroke:#35F,stroke-width:4px
    click P0 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#set-up-participant"
    click P2 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#electrodes"
    click P4 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-patient-reg-resume-patient-registration"
    click P5 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#acquire-landmarks"
    click P6 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#surface-registration"
    click P7 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#coil-calibration"
    click P8 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-checkcamera-check-camera-polaris-spectra-p7-position-toolbar"
    click P9 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#set-up-to-find-motor-hotspot"
    click P10 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-stimulation-stimulation-lower-control-area"
    click P11 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-entrytarget-choose-entry-target-lower-control-area"
    click P12 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-navigation-set-up-navigation-lower-control-area"
    click P13 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-stimulator-stimulator"
    click P14 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#magpro-settings"
    click P16 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-spike-start-sampling-with-spike-2"
    click P17 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#adjust-the-y-axis-spike-2"
    click P17a "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#locate-the-motor-hotspot"
    click P18 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#icon-pest-pest"
    click P19 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#locate-the-motor-hotspot"
    click P21 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#locate-the-motor-hotspot"
    click P23 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#test-threshold-for-3-6"
    click P26 "https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/tms.html#tbs-theta-burst-stimulation"
    P0["Set up Participant"]-->P1("Reference Tracker on forehead")
    P0-->P2(Electrodes)
    P1a-->P3(Earplugs)
    P0-->P1a(Paperwork)
    P1-->P4("Resume Landmark Patient Reg")
    P4-->P5("Acquire Landmarks")
    P5-->P6("Surface Reg")
    P6-->P7("Coil Calibration")
    P7-->P8("Camera Check")
    P13-->P14("Magpro to A")
    P7-->P14("Magpro to A")
    P14-->P15("Demonstrate coil stimulation")
    P8-->P9("Navigation Mode")
    P9-->P10("Stimulation Panel")
    P10-.-P11("Entry/Target Panel")
    P11-.-P12("Navigation Panel")
    P12-.-P13("Stimulator Panel")
    P2-->P16("Start sampling with Spike")
    P16-->P17("Adjust Y-axis")
    P17-->P17a("Locate Motor Hotspot")
    P17a-->P18("Pest: Determine RMT")
    P18-->P19("Set Amplitude")
    P19-->P20("Y or N to Pest")
    P21("Adjust Amplitude")-->P19
    P20-->P21
    P21-->P22("Pest Finishes")
    P22-->P23("Test 3/6 Threshold")
    P23-->P24("70% RMT=TBS Threshold")
    P24-->P25
    P15-->P25("Change Coil and MagPro Setup")
    P25-->P26("Apply TBS")


|icon_spike| Spike2 Setup (MEP computer)
********************************************

Click the Spike2 icon to start the program. Spike2 is used to track muscle contractions from the electrodes you will place on the participant's hand.

New Data Document
====================

|spike2_newdoc|

* For each session, create a new Data Document:
* Click the Spike2 icon on the MEP computer desktop.
* File ➜ New ➜ Data Document ➜ OK

----

Apply Resource File
=========================

.. figure:: /pictures/TMS_Spike2_ResourceFile.png

  Apply the resource file: File ➜ Resource Files ➜ Apply Resource File ➜ *MEPactive_RMS_resource.s2rx* (in My Documents\\Spike7)

----

TMS-EMG Configuration
===============================

|spike2_interface|

* If the script bar is not visible, then select **Script ➜ Script bar** from the menu
*	Click View ➜ Toolbar (this shows us the *Start* and *Stop* buttons we want later)
*	Press TMS-EMG on the Script bar
*	Start Time: Cursor 1
*	End Time: Cursor 2
*	Measurement: 10 Peak to Peak
* OK

.. Note:: Cursor 1 and Cursor 2 may not be visible after hitting okay. Don't worry, they are present but hidden.

----

.. figure:: /pictures/TMS_Spike2_Interface2.png

  You should see three windows: the main EMG Reading Window, a Measurement Text window, and a Log.

Create a participant folder. This is where we will eventually save output from the main Spike EMG Reading window and the Log window.

-----

|icon_setupsession| Set up Session (Localite computer, Toolbar)
*******************************************************************

* Optimally, you should have a T1-weighted scan with ~1mm isotropic voxels. You have several options for providing an anatomical scan (choose only one option, usually DICOMS):

  #. Bring DICOMS direct from the scanner
  #. Bring a Nifti volume
  #. If your participant is returning for additional sessions, you can load a bookmark from the planning you did last time. From the Menu, choose **Load previous bookmark in New Session**. This will allow you to skip ahead to :ref:`participant setup <setup_participant>`. By loading the bookmark in a new session, you will have a separate folder for each session, which makes data mangement cleaner.
  #. If you don't have a scan, you can use the standard MNI scan available in TMS-Navigator

* For option ``1`` TMS Navigator looks for a folder named **DICOMDIR** in your **Patient Folder** and will show you the DICOM images within it.
* For option ``2``, ensure that a folder containing the NIFTI anatomical file(s) is available.

.. figure:: /pictures/TMS_dir_structure.png

  This is TMS Navigator's preferred directory structure: The **Patient Folder** (called ``Aneta Lab`` here) is the folder in which you store **all** of your patients, **not the folder for an individual patient**! **DICOMDIR** (especially relevant if you wish to load DICOMS) is under the Patient Folder and contains all of the DICOM images.  **There should be no subdirectories in DICOMDIR!** All of the DICOM images are at the same level.

-----


.. figure:: /pictures/TMS_Dicom-load1.png

  Ensure you are in the Patient Folder for your study (e.g., ``Aneta Lab``). Select the correct Base Volume Format (left). In this figure, we chose ``DICOM``. Provide a session name. The individual patient folder gets created in the **Enter Patient Demographics** step described below. At that time, the session will be correctly placed inside the individual patient folder.

-----

Load Anatomical Image: Option 1 DICOMS
==========================================

In general, you will load anatomical images directly from the scanner.  This means they will be in DICOM format.  TMS Navigator is specially suited to working with DICOM images.


.. figure:: /pictures/TMS_Dicom-load2.png

  The **Load DICOM Volume** interface: TMS Navigator looks for the specially named folder **DICOMDIR**, reads the contents and parses the DICOM headers to extract information about the patients, studies and series available. You can view the images by checking **Preview** on the bottom left of the interface. Ensure you are in the correct folder! Do you see your patient numbers?  You may need to ``Set Folder`` to point to your own ``DICOMDIR``.

-----

Load Anatomical Image: Option 2 NIFTI
==========================================

.. figure:: /pictures/TMS_Nifti_load.png

  **Left**: If you load a NIfTI volume, you must choose the NIfTI file (\*.nii or \*.hdr) and open it from a location of your choosing.
  **Right**: For a Nifti file, you must specify a NIfTI Volume Transformation: Method 2 (qform) → OK. The Method (qform or sform) is specified in the TMS Navigator preferences, and is currently set to qform.

-----

Enter Patient Demographics
============================

Regardless of whether you loaded DICOMS or NIFTIs, you need to check that your patient demographics information is correct.

.. figure:: /pictures/TMS_Input-Patient.png

  Do one of the following: 1) Select an existing patient from the **Patient List** (top) or 2) Enter new patient data: birthdate=today, name=ID; Sex. OK. If this is a new patient, TMS Navigator creates a folder for the individual patient based on the **PatientID**, **PatientBirthDate**, **PatientName** and the software's **Application Instance UID** (see ``Folder`` at the bottom of the figure). The individual patient folder name gets added to PatientList.xml in the Patient Folder (e.g., ``Aneta Lab``) and will become available in the **Patient List** next time. If the patient already exists, then the new session will be added to that patient's folder.

.. Warning:: If you do not select a patient from the patient list, TMS Navigator will create a second individual patient folder!  This is true even if the patient already exists and even though the correct information is displayed in the **Patient Data** section!  **So, make sure you choose your patient from the patient list at the top (unless it is really a new patient).**

-----

Surface Threshold
======================

.. figure:: /pictures/TMS_HeadSurface.png

  Adjust Surface Threshold (usually ~50) with the threshold slider. Check that whole brain is included in each view plane. Spots outside the head are preferable to holes in the head. Click **OK**. TMS Navigator needs an accurate estimation of where the surface of the head is relative to the brain inside.

------

|icon_patient_reg| Patient Registration Landmark Setup (Toolbar)
**********************************************************************

* In addition to surface registration, specific landmarks help the software track location accurately
* Define Registration Markers (upper right in figure below): Marker List ``+`` for each new marker:
* \(1) L_ear, (2) L_eye, (3) Nasion, (4) R_eye, and (5) R_ear
* Double check colors and locations.
* **Cancel** (Because the participant is not here to be registered).

.. figure:: /pictures/TMS_Patient-Registration.png

  Markers and their colors have been defined in the Marker list (upper right).  Each marker (colored ball) has been set to a location on the head. **Moving the cursor**: Either drag one of the 2D views under the cursor or double-click a tickmark on the cursor to move to that location. Here the left ear marker is selected in the list and display. The marker could be moved by selecting a different location in one of the 2D views and clicking *Set Marker 1* (red rectangle).

-------

|icon_brainseg| Brain Segmentation (Toolbar)
*****************************************************

* Brain segmentation creates a 3D image of the brain which is useful for placing the grid correctly during Talairach definition. The brain segmentation also provides us with a nice view of the sulci and gyri of the brain which facilitates locationg targets.
* Click the cursor in the middle of a large region of WM ➜ Calculate (red rectangle).
* If the cerebellum and/or spinal cord is not included, click each and calculate again.
* Try Defaults, else Range=7-34; Sensitivity=12.
* Increased range ➜ Increased coverage
* You can reset segmentation and start over.

.. figure:: /pictures/TMS_BrainSegmentation.png

  The cerebellum was not successfully segmented in this image. To include the cerebellum in the red mask, click the cerebellum and choose *calculate* (upper right) again.

----

|icon_bc|  Brightness and Contrast (Lower Control Area)
==========================================================

Adjust window width (contrast) and window center:

* Reduced width (range) ➜ increased contrast
* Decreased center ➜ increased brightness

|icon_peelbrain| Peel surface (Lower Control Area)
========================================================

If gyri/sulci are cloudy then peel the brain surface (~3mm)

------

Define Talairach (Menu)
***************************

We define Talairach coordinates so that locations in the individual brain can be located in a standard coordinate system. Defining Talairach for each participant is crucial to using the MNI planning that we set up earlier.

On the menu:

* File ➜ Talairach Definition ➜ Setting markers: *Comissura anterior*, *Comissura posterior*,
* *Point of Falx cerebri* ➜ OK (for falx: Align with AC, Selected location defines top of brain).
* Next

.. figure:: /pictures/TMS_Talairach-def.png

  A sagittal view of the three points you need to find for talairaching: Anterior Commisure (AC) in yellow; Posterior Commisure (PC) in pink; Falx Cerebri (defines top edge of brain tissue at the midline) in cyan.

.. figure:: /pictures/TMS_Talairach-def2.png

  Top: The **anterior commisure** (AC) is displayed in the yellow circle.  It is a little bridge of tissue between hemispheres. The sagittal view displays a typical cross-section of the AC.
  Bottom: The **posterior commisure** (PC) is displayed in the pink circle.  It is also a  little bridge of tissue between hemispheres, but it is typically more difficult to locate than the AC. You probably need multiple views to locate the PC.

.. figure:: /pictures/TMS_Talairach.png

  Drag edges of cage to tightly encompass whole brain+cerebellum. Use 3D image to make sure no brain is outside the cage grid. OK.

.. Note:: Although we use a simple talairaching definition to estimate standard coordinates, we can freely choose either MNI or Talairach coordinates when we plan targets.  All we have to do is **ensure we have explicitly selected which coordinate system we are using when we define the MNI Planning**. This `website <http://sprout022.sprout.yale.edu/mni2tal/mni2tal.html>`_ can help you translate between MNI and Talairach coordinates if necessary.

-----

Load MNI Planning Targets
****************************

Now that the participant's brain is registered to standard space, you can load a saved MNI Planning list, e.g., File ➜ MNI Planning ➜ Kielar lab ➜ Load (Load standard space targets)

Rotation and Entry for each Target
************************************

For each target defined with MNI planning, we need to know the corresponding location for stimulation on the surface of the head.

In the lower control panel:

* |icon_entrytarget| Click the Entry/Target icon to view a list of your loaded targets.
* |icon_planning| Click the Planning icon to **Define Rotation** and **Calculate Entry**. The entry is the location on the head where the coil must be applied to stimulate the desired target.

.. figure:: /pictures/TMS_target_rotation_entry.png

  **Left**: E=Entry; T=Target. Click color squares to set colors (you may want the entry and target to be the same color). ``-`` removes an entry; ``+`` adds an entry. An open eye shows the Entry/Target in the display.
  **Right**: For each target in the Entry/Target List (but see :ref:`Hand Knob <handknob>`), set the rotation and calculate the entry.

In the Planning Control Panel:

* Check **Define rotation**.
* Set rotation to 45 degrees in the left hemisphere and 315 degrees in the right hemiphere.
* Click **Calculate Entry** in the Planning panel.
* If you have reason to set a different rotation value, you certainly should, but the above values are typical.

----

.. _handknob:

The Hand Knob
================

.. figure:: /pictures/TMS_HandKnob.png

  It is likely that the hand knob will not be located correctly, though it should be close. Choose the hand knob in your entry target list and then double-click the correct location on the axial display (**red dot** on the omicron-shaped gyrus) using the guide in the figure above.  Choose **Set Target** and ensure that you define rotation and calculate entry in the :ref:`Planning Control Panel <planning_controlpanel>`.

----

.. _setup_participant:

Set up Participant
********************

* **Paperwork** Explain the study, have the participant read and sign any consent forms, and fill out the MRI and TMS safety screening forms. Double check for contraindications!
* Provide **earplugs** to the participant to protect their hearing from the noise of the coil.
* Place a **reference tracker** on the participant’s forehead. The reference tracker is available in two forms: a stick-on version, and a headband version. In general, the **stick-on reference tracker is to be preferred** as it is better tolerated and more reliable.  Depending on the stimulation site, you might have to use the headband (if it is not possible to place reference on forehead). If using the headband, make sure it is not too tight (can cause headaches) and not too loose (can cause the reference tracker to shift).


Electrodes
==============

**Electrodes** are placed on the participant's hand to establish the motor threshold. Choose the correct hand (e.g., the right hand if we are stimulating the left hemisphere.

.. figure:: /pictures/TMS_hand_muscles.png

  Common muscles to choose are the :abbr:`FDI (First Dorsal Interosseous)`, the :abbr:`ADM (Abductor Digiti Minimi)`, and the :abbr:`APB (Abductor Pollicis Brevis)`;


.. figure:: /pictures/TMS_Electrode-Placement.png

  Electrode placement illustrated for the APB: Red EMG electrode ➜ thumb’s muscle belly (Abductor Pollicis Brevis); White EMG electrode ➜ thumb; Black EMG electrode ➜ back of wrist (Near Ulnar Styloid) for grounding.

.. Note:: No matter the selected muscle, place electrodes as follows: red = muscle belly, white = tendon/bone (lateral face of selected finger), black = bony protuberance of ulnar head for grounding. Before placing the electrodes, you can ask the participant to flex the targeted muscle while palpating the muscle belly. You should feel the muscle contract (contracted muscle = firm), which will inform optimal placement of the muscle belly electrode. For the APB, illustrated here, have the participant repeatedly touch the thumb to the pinky to find the muscle belly.

* Demonstrate the pulse at lower intensity (30) on participant’s arm (as long as there is no metal in the arm) and then the head (45).
* Ask them to relax their hand.

-----

|icon_patient_reg| Resume Patient Registration
=================================================

Now that the participant is here and has a reference tracker affixed, we need to yoke the real world landmarks and head surface to the MRI data in TMS Navigator.

.. Note:: The display contains four equal sized panels. To change the layout, choose *View* ➜ *Layout* in the menu.  For example, you can maximize the size of panel 4 to make it easier to see the 3D representation.


Acquire Landmarks
---------------------

Point to each landmark on the participant with the pointer (guide the pointer with your finger, covering the tip to prevent slipping/unwanted contact). The pointer location needs to match the landmarks on the MRI scan.  For example, the specified landmark location for the left ear may vary by operator (always aim for left tragus). Ensure consistency between the marked target on the MRI scan and pointer placement. Use the 3D head image as a guide.

.. figure:: /pictures/TMS_Patient-Registration-Active.png

  Acquiring landmarks is only successful if both the **pointer** and the **reference** (on the participant's forehead) are visible (green) in the Lower Toolbar as illustrated here. Step on the blue footpedal to **Acquire** each landmark, or ask the computer operator to press the **Acquire** button (blue button, lower right). After acquiring all five landmarks, press the **Next** button (green button, lower right).

.. figure:: /pictures/TMS_Patient-Registration-RMS.png

  **Left**: TMS-Navigator evaluates the registration quality by providing a :abbr:`RMS (Root Mean Square)` deviation value (red rectangle). In this case, the RMS is 8.64, which is too high.
  **Upper Right**: Select **Surface Registration** to try to improve the RMS. OK.
  **Lower Right**: We are presented with a surface threshold.  It is probably fine, but check it.

Surface Registration
---------------------------

* Place the pointer on head ➜ Blue footpedal down ➜ Drag ➜ Blue footpedal up  ➜  pointer off head.
* Repeat in several directions, left & right (100-300 points).
* Get extra registration markers over the eventual stimulation site.
* Be careful not to push too hard as this can cause discomfort.  You want to keep contact with skin, but just lightly tracing the pointer rather than pressing firmly.
* Click **Next** (green button) when you are finished adding points to recalculate the RMS.
* To accept the new RMS, click **OK** .
* To reject the new RMS, click **Previous**, **Next** and **No**. This should allow you to add more data to the surface registration.
* Hold the pointer so it is touching the head and perpendicular to it. You should see no gap between the pointer and the surface of the head (2D views).

.. figure:: /pictures/TMS_Patient-Registration-SurfaceRegistration.png

  Here we have 100 points (green rectangle, upper right). We have clicked **Next** (green button, lower right) to recalculate the RMS. **Right**: Our RMS is now 3.2 mm. Click the green **OK** button on the **Result Surface Registration** window to accept.

----

.. _coil_calibrate:

Coil Calibration
******************

Calibrate the coil to ensure it is recognized by the camera.

|icon_instrument| Select the **Instrument** icon (Lower Control Area)

-----

.. _planning_controlpanel:

|instrument_controlpanel|

* Instrument selection ➜ Coil 1 (MagVenture C-B60)
* Place the calibration board on the coil and hold it near the participant.
* Tip the board and coil so the camera can see all the reflector balls.
* Calibrate Coil 1…


.. figure:: /pictures/TMS_CoilCalibration.png

  **Left**: C-B60 Coil with aligned calibration board.
  **Right**: When the instrument calibration displays **Active** in green at the top of the screen, click the blue **Calibrate** button, then **OK**. The coil is now calibrated.

----

|icon_checkcamera| Check Camera (Polaris Spectra P7) Position (Toolbar)
***************************************************************************

.. figure:: /pictures/TMS_CheckCamera.png

  |icon_noref| If the Reference icon is red, the reflector balls on the participant's head are not being detected, and you should check the camera position. If detected: pointer = green cross, and the reference tracker = pink cross. Coil visibility can be checked as well (pink crosses). Undetected balls are red dots.

------

Set up to Find Motor Hotspot
********************************

* We have set up the equipment and the participant. We have planned our targets and entries.
* We are going to stimulate the hand knob, and subsequently revise the location of the hand knob using an instrument marker from successful stimulation.
* We'll need the four control panels described below.
* |icon_navigationmode| Switch to Navigation Mode (Upper Control Area)

------

|icon_stimulation| Stimulation (Lower Control Area)
=========================================================

.. figure:: /pictures/TMS_Stimulation-ControlPanel.png

  **Left**: The stimulation control panel. Click **Start** (blue rectangle) [The word **Stop** replaces **Start** on the button. We'll use that later.] Open the **Stimulation Markers** dropdown (red rectangle).
  **Right**: The empty stimulation markers list is displayed. At the top, coil 1 (the small coil C-B60) should be selected. Check **Update during recording** (red rectangle).

.. Note:: If you forget to choose **Update during recording** never fear. The stimulation markers are still being created, you just won't see them populate the list right away. But, you should see them when you press **Stop**.

.. _coil_setup:

|icon_entrytarget| Choose Entry/Target (Lower Control Area)
===============================================================

.. figure:: /pictures/TMS_Entry-Target-ControlPanel2.png

  Select the hand knob from the list.

.. _navigation_panel:

|icon_navigation| Set up Navigation (Lower Control Area)
============================================================

.. figure:: /pictures/TMS_Navigation.png

  **Left**: The Navigation control panel showing the 3D scene dropdown. You can select from the dropdown or cycle through with the associated yellow button.
  **Right**: From top to bottom: 3D Data Set view, Entry/Target Navigation view and Instrument Marker view. Choose **Entry/Target Navigation**.

.. _stimulator:

|icon_stimulator| Stimulator
=================================

.. figure:: /pictures/TMS_Stimulator-ControlPanel.png

    Stimulator Control Panel: This panel allows you to (1) enable a coil, (2) set the coil stimulation amplitude, and (3) start recording from the coil.
    To **enable a coil**, change its status |stimulator_enable| by clicking the button to the right (in the red box).
    You can set the stimulation amplitude in the **Amplitude** textbox. If you use the arrows to set the stimulator amplitude, then the settings will be applied immediately.  If you type an amplitude into the box, then you need to hit enter to apply the settings. You will need this when you stimulate the hand knob with various amplitude values dictated by :ref:`Pest <pest2>`. The coil operator can trigger a pulse with the orange button on the coil.

MagPro Settings
===================

On the MagPro screen, ensure the coil is correctly identified as the C-B60 (the small coil). Turn the Options Wheel to ``A``.  Select **Recall**. After selecting **Recall**, the details of your sequence are displayed in the information area on the left: ``Setup View A``. You may use the **Amplitude Wheel** and **Enable/Disable** button if you prefer these to the Stimulator panel.

.. Warning:: When you enable the coil, amplitude is reset to ``0``! You will need to set amplitude to ``55`` when you try to locate the motor hotspot. If you switch between different methods of setting the amplitude (dial on C-B60 coil, MagPro amplitude wheel and stimulator control panel), the amplitude may jump when you switch back to the previous method. Be cautious about this.

-----

The Motor Hotspot
********************************

* Locating the motor hotspot requires TMS-Navigator on the Localite computer and `Spike 2 <http://ced.co.uk/products/spkovin>`_ on the MEP computer.
* Establishing the :abbr:`RMT (Resting Motor Threshold)` additionally requires `Pest <https://www.clinicalresearcher.org/software.htm>`_ on the MEP machine.


|icon_spike| Start Sampling with Spike 2
===========================================

.. figure:: /pictures/TMS_Spike2_StartSampling.png

  The start button is equivalent to choosing Sample ➜ Start Sampling from the menu (red rectangles).

|icon_spikeslow| Sampling Speed is rather fast, click the slow button in the lower left corner of the Spike2 interface.  About 4 clicks should do it.

|icon_spikespeed| If you go too far, there is an adjacent icon for speeding up the sampling.

Adjust the Y-axis (Spike 2)
------------------------------

.. figure:: /pictures/TMS_Spike2_y-axis.png

  Optimize the view of the readings for both yourself and the participant. Each channel in the Spike window can have its Y-Axis adjusted: either click and drag the axis or right click the Y-Axis and select **Optimize Y-Axis**.

-----

Locate the Motor Hotspot
=============================

* **Set the amplitude** to ``30``. The amplitude can be set in at least 3 different ways:

    * Using the dial on the C-B60 coil
    * Using the amplitude wheel on the Magpro
    * Using the Stimulator control panel in TMS Navigator
* Demonstrate the coil stimulation on the participant's arm at amplitude= ``30`` (as long as there is no metal in the arm), and head at amplitude= ``45``.
* Set the amplitude= ``55``, then navigate to the handknob and stimulate it. You may have to move the coil around to find the correct spot.
* Watch the participant's hand. The thumb, and not some other part of the hand, should move.
* Often this is right where we placed the handknob target, but if that isn't right, we need to move the coil around a bit and try stimulating nearby areas.
* Occasionally someone has a higher threshold and you'll need to increase the amplitude to stimulate the hotspot (e.g., we've seen amplitudes of 75 or even 80, but this is uncommon).
* When you find a robust thumb twitch, press **Stop** on the stimulation control panel.  We are done recording:


.. figure:: /pictures/TMS_Stimulation-ControlPanel-Responses.png

  Select a stimulation from the list that corresponds to a robust thumb movement. Choose **Transform Selection** to open the Instrument markers Control panel and transform your selection into an instrument marker.

.. figure:: /pictures/TMS_InstrumentMarkers-ControlPanel.png

  **Left**: The Instrument Markers Control Panel opens and displays the stimulation you chose above.
  **Right**: Give the selected marker a better name and color.

In the :ref:`Navigation control panel <navigation_panel>`, choose Instr. Marker Navigation to navigate using the instrument marker we just created.

-------


.. _pest2:

|icon_pest| Pest
==========================================

Now that we have identified the motor hotspot, start Pest. Pest is a motor threshold assessment tool. Pest runs some clever algorithms to help choose the optimal stimulation threshold. We will use Pest to stimulate the identified motor hotspot from the previous step.

.. figure:: /pictures/TMS_Pest-Setup-New.png

  Select **Threshold estimation in the range from 20% to 80% stimulator output** (red rectangle) to use the default value of ``45``. Alternatively, it is possible to set a different threshold by selecting **Refinement of raw threshold estimate**, but the default is usually fine.  Press **START**. ``45`` (or whatever threshold you chose) appears in the lower right as shown here. Pest will tell you what coil amplitude to try next. That amplitude can be set, for example, on the :ref:`stimulator panel <stimulator>` on the Localite machine.

-----

.. figure:: /pictures/TMS_Pest-results.png

  Each stimulation applied to the hand knob will either produce a finger twitch or not. If the stimulation produces a twitch, then type ``y`` into Pest. If the stimulation does NOT produce a twitch, then type ``n`` into Pest.  Pest will make a sound if it detects your ``y`` or ``n`` (but it can lag a little, so make sure you wait long enough to hear the acknowleding sound). Pest will suggest the next amplitude to try. Pest displays a record of the stimulations graphically and as a table on the left. Avoid clicking the table as the software will become unresponsive (this appears to be a bug in the Windows version we are using). When Pest has sufficient data, the big number (e.g., ``44`` in this figure) on the lower right will turn red. This is Pest's best guess at the **motor threshold**.

Test Threshold for 3/6
==========================

After identifying Pest's guess at the motor threshold, ensure that 3/6 stimulations at that intensity produce a twitch.

* If we get too few responses, increase the threshold by ``1`` and try again.
* If we get too many responses, decrease the threshold by ``1`` and try again.
* If you get ``4/6`` and then decrease and get ``2/6``, then your final value is the ``4/6`` value.
* After we establish the :abbr:`RMT (Resting Motor Threshold)` we calculate 70% of that value. 70% of RMT will be our :abbr:`TBS (Theta Burst Stimulation)` intensity for both :abbr:`cTBS (continuous Theta Burst Stimulation)` and  :abbr:`iTBS (intermittent Theta Burst Stimulation)`.

|icon_spike| Spike2: Quit and Stop (Do NOT close)
=========================================================

|spike2_quit|

.. Note:: When recording is stopped, it cannot be resumed. We will wait until later to save the MEP data.

* Remove electrodes from participant
* For now press the quit button (illustrated in the figure) and the stop button on the Toolbar.
* Later, when we finish the session we will save data from Spike.
* For now, you **must not close Spike**.

------

.. _TBS:

TBS (Theta Burst Stimulation)
*************************************

* It is time to change the coil from the small C-B60 to the large Cool-B65 we will use for :abbr:`TBS (Theta Burst Stimulation)`.  As soon as you unplug the small coil, the Localite computer will prompt you to *switch or keep calibration*.  It does not matter what you select, or if you ignore the prompt until the large coil is plugged in.
* Ensure the cooling unit is on.
* You are going to apply either :abbr:`iTBS (intermittant Theta Burst Stimulation)` which increases cortical excitability or :abbr:`cTBS (continuous Theta Burst Stimulation)` which decreases cortical excitability.
* Check the positioning of the coil tracker balls to ensure they won't be in the way while you are applying the stimulation to the participant.  You can move them but you should do so before you calibrate the coil.
* :ref:`Calibrate the coil <coil_calibrate>`. This time choose **Calibrate Coil 2**. The calibration screen should recognize that the coil is now the Cool-B65.
* Apply the :ref:`coil setup instructions <coil_setup>` to Coil 2 (the Cool-B65)
* On the MagPro screen press the **Main** button.
* **Main** will be replaced by the  **Timing** on the bottom left of the screen.
* Turn the knob to ``Y`` for iTBS or ``C`` for cTBS. Press **Recall**.
* Press **Timing** to see the timing details.  The ``START/STOP`` softkey on the lower right is enabled.  Now the pulse train can be triggered from the **Stimulator** panel using the **Start/Stop Train button**
* Navigate to the target of stimulation (something other than the hand knob, we presume).

.. figure:: /pictures/TMS_hardware_MagPro_iTBS_timing.png

  The MagPro screen with 32% amplitude, and the Cool-B65 coil enabled: The status area now reports the coil temperature and available stimuli. The Timing screen is displayed, instead of the Main screen. This is the timing for ``Y``, the iTBS sequence. Note the long (8 sec) intertrain interval and the STOP softkey on the lower right.

----

.. figure:: /pictures/TMS_Coil2-Enabled-StimulatorPanel.png

  The stimulator panel with the Cool-B65 enabled: Because the Timing screen is displayed on the MagPro, the **Start/Stop Train** button can be used to initiate the pulse train. Of course, before initiating the pulse train, the amplitude should be set to the value you calculated earlier as 70% of the resting motor threshold.

------

The Cool-B65 coil is heavy which makes it difficult to hold steady for the requisite amount of time. The heavy coil may also tend to move the participant's head as it rests against it. You may find it helpful to have the participant use the chinrest (there is a soft blue squishy disk to make it more tolerable).

.. figure:: /pictures/TMS_hardware_chinrest.png

  **Left**: In its vertical poisition the chinrest is a good height for the average participant.  It can easily be extended to be taller.  If the chinrest is too tall for your participant, you can tip its angle down to any height (using the adjustment marked with the red arrow).
  **Right**: The chinrest tipped at an angle.

----

Wrap Up the Session
**************************

* You want to save data from both the Localite and MEP machines to your external device.
* You also want to turn off hardware.

Save Data
=============

**Localite Data** Save the participant folder you created earlier to your external device.
**MEP Data** Save data from Spike2 and Pest to your external device.

* **Spike2**

  * **Save Main EMG Reading Window**: Make sure that the **focus is on the main EMG Reading Window** and click *Save* in the upper left to save it to the appropriate participant folder. If the file has the \*.smr extension, then you know you saved the right window.
  * **Save Log**: Save the log by clicking in the log window and following the same steps (but the output will be a \*.txt file).
  * Ensure that you have named both files using your protocol and that they are in the participant folder you created.
  * Copy the participant folder to your external drive.
  * You can close Spike2.
* **Pest**

  * Save the Pest data to the participant folder.

----

Turn off Hardware
===========================

* **MEP** Turn off the amplifier and data acquisition devices.
* **Coil** Disconnect the large coil, and reconnect the smaller coil.
* **MagPro** Turn off the Cooling Unit and MagPro.
* **Localite Computer** Turn off Localite computer.

.. Note:: Order is not terribly important here. The MEP equipment is separate from the Coil/Localite/MagPro. It is probably best to turn off the Localite machine last (but I'm not sure why I say that).

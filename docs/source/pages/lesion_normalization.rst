.. _lesion_normalization:

======================
Lesion Normalization
======================

| Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2019_06_25
| Date Updated: 2019_11_14
| Tags: anat, lesion, normalization


.. toctree::
   :hidden:
   :maxdepth: 4

   /pages/ants_anat_normalization-lesion
   /pages/fsl_anat_normalization-lesion
   /pages/spm_anat_normalization-lesion
   /pages/spm_anat_normalization-lesion-tpm
   /pages/spm_anat_normalization-lesion-old
   /pages/spm_anat_normalization-rev


A test of several normalization methods was completed with 27 subjects selected from the publicly available dataset described here:

.. _Liew_reference:

Liew, S.-L., Anglin, J. M., Banks, N. W., Sondag, M., Ito, K. L., Kim, H., et al. (2018). A large, open source dataset of stroke anatomical brain images and manual lesion segmentations. Scientific Data, 5, 180011–11. http://doi.org/10.1038/sdata.2018.11

A pdf presentation comparing these methods is available here: `10_things_lesions.pdf <https://osf.io/xz9qs/>`_ or `10 things lesions.pptx: <https://osf.io/ts72j/>`_ The dataset was sufficiently large, homogenous, and well-documented to provide a grounding for comparing techniques.

Exploratory Comparisons as Tableau Dashboards
**************************************************

Tableau dashboards are interactive and dynamic.  In the two dashboards described below, you can change which subjects and which methods you are viewing with the filters to the right of the dashboard. Mousing over features (i.e., bars in bar charts, cells in tables, lines and circles in the scatterplot) displays more information.

Brain Mask Matching Dashboard
==================================

This first dashboard explores how each method succeeds in matching the patient's brain mask to the standard brain mask. Because we want to compare patients and determine whether their lesions are in the same place, it is important for their masks to match the standard (and therefore, each other).  Obviously this is a very rough measure of how completely the brains are matched in standard space.  Note that **FSL** does especially well on this measure: high Jaccard overlap values in the hotspot table, high mean value in the top right bar chart and low standard deviation in the bottom right bar chart:
`Brain Mask Matching`_

.. _Brain Mask Matching: https://public.tableau.com/profile/dianne.patterson#!/vizhome/brain_math/Dashboard_Brain_mask_match?publish=yes

Lesion Preservation Dashboard
=================================

The second dashboard explores how well lesion volume is maintained after normalization.  For example, if the lesion occupied 15% of the brain mask in the native space image and 18% of the brain mask in standard space, then we'd say there was a 3% increase (pink cell in table) in the size of the lesion as the result of the normalization process. What we want is for lesion volume to stay the same (grey cells).  Scroll down in the dashboard table to see the few instances that exceed 1% (sub-027 and sub-030).

What we fear is that larger lesions are more affected by the normalization process than smaller lesions.  At the bottom of the dashboard is a scatterplot with best fit linear model lines for each method. Indeed, there is a significant effect of lesion volume on the normalization process for every method except **ant2**. However, the change in lesion volume that results from normalization is VERY small. This can be seen by examining the standard deviation and mean of the changes (shown at the top of the dashboard): mean < 1% ; std < 2% for all the methods.
`Lesion Volume Preservation`_

.. _Lesion Volume Preservation: https://public.tableau.com/profile/dianne.patterson#!/vizhome/lesion_change/Dashboard_LesionPreservation

Overview of Results
**************************

In general, modern methods work well (ant2, ant3, fsl, spm_head, spm_tpm_head) though several important points deserve notice:

* SPM12 does MUCH better with the whole head than with the extracted brain.
* There isn't any great advantage to providing SPM12 with a lesion mask.
* The SPM12 approach to normalization is very different than ANTS or FSL.
* ANTS and FSL appreciate a GOOD brain extraction.
* Brain extraction is tricky for brains with big lesions. See the optiBET discussion on the :ref:`FSL Lesion Normalization <fsl_lesion_norm>` page.

Recommended Approach
************************

Generally, you can follow this description :ref:`SPM12 Normalization <spm_lesion_norm>`.
However, if you have not yet created a lesion mask, then you can implement the steps described in the :ref:`Segmentation section <spm_seg>` and the :ref:`Normalise Anatomical Image section <spm_norm_anat>`, but skip the step where you normalize the lesion mask (because, after all, in this scenario you do not have a lesion mask).

If you are interested in working with a tool like tractotron, then you'll need to crop your result files into FSL standard space.  This is easily accomplished with the `spm2fsl.sh <https://bitbucket.org/dpat/tools/raw/master/LIBRARY/spm2fsl.sh>`_ script. A description is available here :ref:`Convert Images from SPM MNI to FSL MNI <SPM2FSL>`.

In addition, you may find that SPM introduces NaNs into your image.  NaNs can cause trouble in many programs, so :ref:`Ensure Zeros, not NaNs <zeros_not_NaNs>`.

Normalization Methods Compared
***********************************

ANTS
========

* See :ref:`ANTS Lesion Normalization <ants_lesion_norm>`

* **ant1** ants version 1.X normalization routine. Start data is the result of optibet.sh (see FSL section below), hence skull-stripped. No source weighting. Normalized to MNI152_T1_2mm_brain.nii.gz.

* **ant2** ants version 2.X normalization routine. Start data is the result of optibet.sh, hence skull-stripped. Source weighting. Normalized to MNI152_T1_2mm_brain.nii.gz.

* **ant3** Same as ant2, except start data was the final result of fsl_anat_alt, rather than the result of optibet. Lesion preservation comparisons are based on the final fsl_anat_alt mask.

FSL
======

* See :ref:`FSL Lesion Normalization <fsl_lesion_norm>`

* **fsl** Runs modified fsl_anat to turn on the lesionmask option, and to run optibet early on to get a much better initial skull-strip.  This allowed us to use the skull-stripped image for segmentation and normalization.  Tests showed optibet did a much better job of skull-stripping than bet. Source weighting. Normalized to MNI152_T1_2mm_brain.nii.gz. Lesion preservation comparisons are based on the final fsl_anat_alt mask.

SPM
======

SPM: Old
----------

* See :ref:`SPM Old Normalization <spm_lesion_norm_old>`

* **spm_old** Old normalization. Normalized to Template Image/T1.nii. source weighting, whole head.

* **spm_old_nosw** Old normalization. Normalized to Template Image/T1.nii. No source weighting. whole head.

SPM12
-------

* See :ref:`SPM12 Normalization <spm_lesion_norm>`

* **spm_brain** spm12 normalization starting with the optibet result (hence skull-stripped). No source weighting.

* **spm_head** spm12 normalization starting with the whole head. No source weighting.

SPM12 TPM
-----------

* See :ref:`SPM12 Normalization with TPM <spm_lesion_norm_tpm>`

* **spm_tpm_brain** spm12 normalization starting with the optibet result (hence skull-stripped). Defines lesion as tissue type.
* **spm_tpm_head** normalization starting with whole head. Defines lesion as tissue type.

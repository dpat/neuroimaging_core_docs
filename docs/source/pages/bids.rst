.. _bids:

=====
BIDS
=====

| Author/Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2018_12_07
| Date Updated: 2024_11_14
| Tags: BIDS, standards, containers
| OS: UNIX (e.g., Mac or Linux)


.. toctree::
   :hidden:
   :maxdepth: 3

   /pages/bids_containers


Learn about this two-pronged project which describes (1) a standard file and directory naming structure for neuroimaging datasets, and (2) containerized apps that take assume this naming structure to great advantage. Below I provide useful links, including descriptions of software we've actually tried here at the U of A and related gotchas and tips.

The U of A Neuroimaging Singularity containers are stored in `contrib/singularity/shared`.  This folder is only visible if you are in interactive mode. Please contact one of the administrators (Dianne Patterson, Chidi Ugonna or Adam Raikes) if you'd like to see new or updated containers added to this shared area.  

Main Links
****************

* `BIDS Read-the-Docs <https://bids-website.readthedocs.io/en/latest/index.html>`_
* `BIDS Apps <https://bids-website.readthedocs.io/en/latest/index.html>`_
* `BIDS tutorials <https://bids-website.readthedocs.io/en/latest/getting_started/>`_
* `BIDS Specification <https://bids-specification.readthedocs.io/en/stable/>`_


BIDS compatible datasets
===========================

* `Downloadable examples <https://github.com/bids-standard/bids-examples>`_

Relevant Papers
*******************

* Gorgolewski, K. J., Auer, T., Calhoun, V. D., Craddock, R. C., Das, S., Duff, E. P., et al. (2016). The brain imaging data structure, a format for organizing and describing outputs of neuroimaging experiments. Scientific Data, 3, 160044–9. http://doi.org/10.1038/sdata.2016.44 https://www.nature.com/articles/sdata201644
* Gorgolewski, K. J., Alfaro-Almagro, F., Auer, T., Bellec, P., Capotă, M., Chakravarty, M. M., et al. (2017). BIDS apps: Improving ease of use, accessibility, and reproducibility of neuroimaging data analysis methods. PLoS Computational Biology, 13(3), e1005209–16. http://doi.org/10.1371/journal.pcbi.1005209 http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1005209

Educational and Related Materials
**************************************

* `The BIDS folder hierarchy <https://github.com/INCF/bids-starter-kit/wiki/The-BIDS-folder-hierarchy>`_
* `Join BIDS google groups <https://groups.google.com/forum/#!forum/bids-discussion>`_
* `An introduction to Docker for BIDS users <https://neurohackweek.github.io/docker-for-scientists/>`_
* `Video of docker-bids workshop by Chris Gorgolewski (links to his other videos on right) <https://www.slideshare.net/chrisfilo1/docker-for-scientists>`_
* `Dataflows for BIDS <https://neurohackweek.github.io/lesson-dataflows/>`_
* `BIDS templates <https://github.com/BIDS-Apps/dockerfile-templates>`_
* `A presentation on the BIDS data format and HeuDiConv for DICOM conversion <http://nipy.org/workshops/2017-03-boston/lectures/bids-heudiconv/#1>`_
* `fmriprep documentation <https://fmriprep.readthedocs.io/en/stable/index.html>`_


Creating BIDS Datasets
***************************

Tools available for creating BIDS datasets focus on converting DICOMS to NIFTI and creating the accompanying json sidecar files for each NIFTI image.  If you don't have the original images off the scanner, there are not currently any good solutions for converting your data to BIDS.  Part of the reason you should start with the DICOM (i.e., dcm or IMA images here at the U of A), is that there is considerable information in the DICOM headers that does not get transferred to the NIFTI headers.  If you convert the DICOMs to BIDS, then this extra (deidentified) information gets saved in the json sidecar. The defacto tool for conversion is dcm2niix by Chris Rorden: `download dcm2niix <https://github.com/rordenlab/dcm2niix/releases>`_  and `read more about dcm2niix <https://www.nitrc.org/plugins/mwiki/index.php/dcm2nii:MainPage>`_. dcm2niix produces the BIDS format json sidecar files, handles dcm and IMA files, and correctly saves slice timing for multiband data (which is not preserved in the NIFTI header).

.. Note:: Neither dcm2niix nor :ref:`HeuDiConv <heudiconv>` will name your output files in a bids compliant way by default. You must look at the BIDS specification and figure out the naming. That said, I try to provide examples and guidance for UofA data on the :ref:`HeuDiConv <heudiconv>` page.

.. Note:: The BIDS specification is under active development.  In general, if your data is named in a compliant way today, it'll still be okay tomorrow.  However, tomorrow may bring additional options.


.. toctree::
   :hidden:
   :maxdepth: 3

   /pages/bids-validator
   /pages/heudiconv

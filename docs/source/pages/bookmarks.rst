.. _bookmarks:

================
Bookmarks
================
| Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2018_09_09
| Date Updated: 2019_10_31
| Tags:

Also see the :ref:`Glossary <glossary>` page.

.. _atlas_bookmarks:

Brain Atlases
==============

* `Brain Info <http://braininfo.rprc.washington.edu/>`_ is designed to help you identify structures in the brain.
* `CORTICAL ATLAS PARCELLATIONS IN MNI SPACE <https://www.lead-dbs.org/helpsupport/knowledge-base/atlasesresources/cortical-atlas-parcellations-mni-space/>`_
* `NIF <https://neuinfo.org/>`_, the Neuroscience Information framework, is a dynamic inventory of web-based neuroscience resources.
* `OBART <http://obart.brainarchitecture.org/obart/>`_ The Online Brain Atlas Reconciliation Tool (OBART) aims to provide a quantitative solution to the so-called neuroanatomical nomenclature problem by comparing overlap relations between regions defined as spatial entities in different digital human brain atlases.

.. _computational_resources:

Computational Resources
=========================

* `BIDS <https://sites.google.com/a/email.arizona.edu/bmw/resources/bids>`_ Brain Imaging Data Structure links
* `Cyverse <https://sites.google.com/a/email.arizona.edu/bmw/resources/cyverse>`_ Cyberinfrastructure supporting online customizable virtual machines (Atmosphere), containerized apps (Discovery Environment) and data storage (Data Commons). You must request access to each service separately.
* `Docker Containers <https://www.docker.com/>`_
* `GIT Repositories <https://www.gitkraken.com/learn-git>`_ An excellent set of tutorials introducing the concept of code repositories and how to use them.  Examples use Gitkraken which is a graphical git client available for free to academics.
* `HPC Documentation <https://docs.hpc.arizona.edu/>`_ University of Arizona High Performance Computing Documentation

  * `HPC Account Creation <https://docs.hpc.arizona.edu/display/UAHPC/Account+Creation>`_
  * `OOD <https://ood.hpc.arizona.edu/pun/sys/dashboard>`_ Open On Demand Dashboard to interact with HPC resources.
  * `HPC Introduction <https://drive.google.com/file/d/1DdfxMFFlwWWvQq4j9Vlab36kINdeyxzG/view>`_

* `Jupyter Org <http://jupyter.org/try>`_ A free online unix terminal and Jupyter Notebook server.
* `OpenNeuro <https://openneuro.org/>`_ A site that implements BIDS apps in an easily accessible gui interface. The catch is that you have to make your data public after a couple of years.
* `REDCap <https://cb2.uahs.arizona.edu/services-tools/surveys-clinical-databases-redcap>`_ A fully IRB-approved HIPAA-compliant web database application freely available to U of A researchers for capturing and querying data (e.g., we could have potential participants fill out their MRI screening here).
* `Research Bazaar Unix Introduction <https://github.com/sjmillerAZ/ResBaz_UNIXIntro/blob/master/Intro-to-shell.md>`_
* `SCIF <https://sci-f.github.io/>`_ The Scientific Filesystem
* `Software Carpentry <https://software-carpentry.org/lessons/index.html>`_
* `Software and Data Carpentry: University of Arizona <https://github.com/UA-Carpentries-Workshops>`_
* `Singularity Containers <https://www.sylabs.io/docs/>`_

fMRI Databases
===============

* The `Brede Database <http://neuro.imm.dtu.dk/services/brededatabase/>`_ allows you to look up a brain region and discover what tasks activate it.
* `Neurosynth <http://neurosynth.org/>`_ is a platform that automatically synthesizes fMRI results across many studies.
* `OpenNeuro <https://openneuro.org/>`_ See *Computational Resources* above.  This is also a site to get publicly available BIDS compliant datasets.

Instructional: Neuroimaging Software and Concepts
======================================================

* `Andy's Brain Book <https://andysbrainbook.readthedocs.io/en/latest/>`_ Andrew Jahn's Read The Docs pages
* `Andy's Brain Blog <https://www.andysbrainblog.com/>`_ Online tutorials and videos on a variety of topics: Afni, Eprime, Freesurfer, FSL, FSLeyes, Unix
* `FMRIF Summer 2017 Course Materials <https://fmrif.nimh.nih.gov/public/fmri-course/fmri-course-summer-2017>`_ fMRI powerpoints and video presentations
* `GIFT <https://osf.io/ubmnr/>`_ Resources related to the GIFT ICA Matlab processing toolbox: presentations, a manual, sample data, a short video etc.
* `Standard fMRI Analysis Tutorials <http://www2.bcs.rochester.edu/sites/raizada/fmri-matlab.html>`_ Rajeed Raizada provides some excellent interactive Matlab tutorials for MVPA (multi-voxel pattern analysis) and SPM fMRI analysis.
* `Resting State Analyis: How To <https://www.ohbmbrainmappingblog.com/blog/ohbm-ondemand-how-to-resting-state-fmri-analysis>`_ Online resource from Jeanette Mumford et al.

MRI Concepts
=============

* `MRI Questions <http://mriquestions.com/index.html>`_ provides articles explaining how MRI works.
* `MR-Tip <https://www.mr-tip.com/serv1.php?type=db>`_ is a glossary of MRI-related terms.

Neuroimaging Tools
====================

* `NITRC <https://www.nitrc.org/>`_, the Neuroimaging Tools and Resource Collaboratory, is a huge database of neuroimaging tools and datasets.

Other
======================

* The `Massive Memory Database <http://cvcl.mit.edu/MM/index.html>`_ is a huge database of images useful for building visual stimulus sets. Images are organized into useful categories, like *state pairs*, *exemplar pairs* etc.
* `Open Brain Consent <https://open-brain-consent.readthedocs.io/en/stable/#>`_ A discussion of how to word your consent form to allow for sharing imaging data.

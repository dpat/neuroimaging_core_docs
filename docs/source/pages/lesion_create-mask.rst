.. _creatingthelesionmask:

==========================
Creating a Lesion Mask
==========================

| Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2019_06_25
| Date Updated: 2019_08_17
| Tags: anat, lesion, stroke, drawing masks, segmentation, header editing, registration

It is difficult to draw lesion masks consistently. Liew et al. (2018) report that "inter-rater reliability was 0.76 ± 0.14, while the intra-rater reliability was 0.84 ± 0.09". Clearly rater reliability is THE major source of variance in lesion masks. We know that some lesions are more difficult to delineate, and we can hypothesize that large lesions are more heterogeneous and more likely to intersect brain and ventricle boundaries, which might make them more difficult to characterize consistently. We also know that acute lesions are more difficult to characterize than chronic lesions (Pelagie Beeson, Ph.D, Personal communication).

A traditional approach to creating lesion masks is provided by the ATLAS project and can be found here: `MRIcron Lesion Tracing <https://github.com/npnl/ATLAS/blob/master/ATLAS_NPNL_LesionTracing_MRIcron_SOP.pdf>`_

.. Warning:: MRIcron assumes isotropic (or near isotropic) voxels.  This means it is not well suited to displaying clinical data which often has a high resolution in-plane, but very thick slices. Although ITK-SNAP can display anisotropic data, the semi-automated routines benefit from isotropic data. See :ref:`Correct Extreme Anisotropy <correct_anisotropy>` below for a description of how to correct anisotropy.

A semi-automated approach using ITK-SNAP can be found here :ref:`ITK-SNAP Segmentation: Lesions <snap_seg>`.  The benefits of ITK-SNAP over MRIcron are described in the Introduction here: :ref:`ITK-SNAP <snap>`.

iPad Drawing Tools
********************

It is difficult to draw for very long with a mouse or trackpad. Wacom tablets are expensive. I have found the following two iPad options to be useful.  Both have pros and cons.  Both are better with the Apple pencil.

* The :ref:`iMango <imango_section>` software is a standalone application for the iPad. $15.
* Another alternative is `Astropad Standard <https://astropad.com/standard/>`_ $30.

  * Astropad requires a connection (wired or wireless) to your primary Mac (which assumes you HAVE a mac).
  * However, Astropad displays any Mac application for use on the iPad.
  * If you like MRIcron or ITK-SNAP, then Astropad provides a inexpensive competitor to the Wacom tablet.

Related Pages
*****************


.. toctree::
   :hidden:
   :maxdepth: 3

   /pages/itk-snap_segmentation-lesion
   /pages/spm_anat_normalization-rev
   /pages/image_processing_tips

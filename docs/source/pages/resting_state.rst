===============
Resting State
===============

| Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2018_12_18
| Date Updated: 2018_06_26
| Tags: Neuroimaging software, func

Introduction
================

Resting state data is fMRI data collected while the subject is at rest, but not sleeping. This often involves staring at a fixation cross, but one group has devised a more engaging activity which nevertheless generates resting state networks. See `the Inscapes article <https://www.ncbi.nlm.nih.gov/pubmed/26241683>`_.  This 7 minute movie can be shown to participants during a resting state scan.  It is available on the task computer at the UofA scanner (on the desktop, and labeled 01_Inscapes_NoScannerSound_h264 1.mp4).  Or you can download the `original inscape movie <http://headspacestudios.org/inscapes.html>`_ with or without scanner sound.


.. toctree::
   :hidden:
   :maxdepth: 3

   /pages/fsl_fmri_restingstate-sbc

.. _ants_lesion_norm:

============================
ANTS
============================


| Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2018_08_10
| Date Updated: 2019_09_18
| Tags: ANTS, normalization, standard space, lesion, anat
| Software: ANTS_2, and probably FSL and optiBET.sh

Data
****

* Images can be zipped or not (i.e., *T1w.nii* and *T1w.nii.gz* are OK).
* A lesion mask with values of 1 in the lesion and 0 elsewhere.
* A template image. In this script we assume the template image is in the FSL directory here: /usr/local/fsl/data/standard/MNI152_T1_2mm_brain.nii.gz.
* A skull-stripped structural (anatomical) T1 weighted image. See :ref:`Skull stripping with optiBET <skull_strip>`

Steps
*****

* **Goal**: Warp a native space lesion mask (and native space anatomical image) into standard MNI space.
* **Requirements**:

  * ANTS installed and working.
  * FSL installed and working.

    * FSL provides the template image (MNI152_T1_2mm_brain.nii.gz)
    * optiBET.sh relies on FSL.

* Place the `ant_reg2.sh <https://bitbucket.org/dpat/tools/raw/master/LIBRARY/ant_reg2.sh>`_  script in your path (left-click to view script; right-click to download).

The script
==========================
**ant_reg2.sh** assumes we are in the directory with the images we need. It further sets the
subject variable ${sub} to be the basename of the directory (you can change this).
ant_reg2.sh expects 2 arguments:

* a T1 anatomical image (skull stripped--optibet.sh suggested)
* a lesion mask (1=lesion; 0=non-lesion)
* e.g. ant_reg2.sh sub-001.nii.gz sub-001_LesionSmooth.nii.gz

The main tools called in **ant_reg2.sh** are antsRegistration and antsApplyTransforms. These replace ANTs and WarpImageMultiTransform respectively. ANTs and WarpImageMultiTransform were called in the old ANTS. Registration is so complex that there are a couple of scripts to implement it, a quick one, antsRegistrationSyNQuick.sh, and a slower one, antsRegistrationSyN.sh. ant_reg2.sh
first creates the inverse lesion mask using the ImageMath function, then calls antsRegistrationSyn.sh with that masking. After creating the affine and warp registrations, they are applied in antsApplyTransforms.  The approach follows the `basic brain mapping example <https://github.com/stnava/BasicBrainMapping/blob/master/bbm.sh>`_.

Alternative approaches
================================

* **ant1** For comparison we ran the old ants tools, ANTs and WarpImageMultiTransform, without source weighting. See `ant_reg1.sh <https://bitbucket.org/dpat/tools/raw/master/LIBRARY/ant_reg1.sh>`_

* **ant3** We also ran the ant2 procedure described using a different brain mask. We used the final result of fsl_anat_alt instead of the result of optibet.  This was motivated by sometimes odd looking brain masks produced by optibet (at least one was clearly wrong). Other than the starting mask, the script was the same as the ant2.sh script. See `ant_reg3.sh <https://bitbucket.org/dpat/tools/raw/master/LIBRARY/ant_reg3.sh>`_.

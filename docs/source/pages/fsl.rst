===
FSL
===

| Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2018_06_14
| Date Updated: 2018_11_14
| Tags: Neuroimaging software, DWI
| OS: UNIX (e.g., Mac or Linux)


.. toctree::
   :hidden:
   :maxdepth: 3

   /pages/fsl_anat_normalization-lesion
   /pages/fsl_fmri_restingstate-sbc
   /pages/image_processing_tips

`FSL <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/>`_  is a general purpose neuroimaging workbench, like AFNI and SPM. FSL excels at handling diffusion weighted imaging.

FSL has special facilities for using Siemens :term:`field maps` for distortion correction of epi images (both dwi and fMRI) using the script `epi_reg <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT/UserGuide#epi_reg>`_ to run boundary based registration (:term:`BBR`). Field maps can be acquired quickly at scan time (about a minute). Using field maps for distortion correction requires correctly identifying some image parameters that are described on the :ref:`glossary` page: :term:`Difference in Echo times`, :term:`Dwell time`, and :term:`Phase Encode Direction`.

In addition, :term:`reverse phase encode` images can be used to enhance distortion correction. Mostly these corrections are used for dwi, where the distortions are especially bad, but some people have successfully used reverse phase encode distortion correction on fMRI. A couple of reverse phase encode B0 volumes can be acquired quickly at scan time (about a minute). Using the reverse phase encode volumes for distortion correction requires the use of :term:`topup`, and optionally :term:`applytopup`. It is also necessary to  correctly identify some image parameters that are described on the :ref:`glossary` page: :term:`Phase Encode Direction` and :term:`Total Readout Time`.

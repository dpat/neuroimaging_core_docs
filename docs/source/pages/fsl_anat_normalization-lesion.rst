.. _fsl_lesion_norm:

========================
FSL Lesion Normalization
========================

| Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2018_07_27
| Date Updated: 2019_07_30
| Tags: FSL, normalization, standard space, lesion, anat
| Software: FSL 5.10 (or similar), The zip file of scripts described below

* **Goal**: Warp a native space T1 weighted image and lesion mask into standard (MNI) space using FSL.
* **Requirements**: You need to have FSL installed and working.  Place the three scripts described below (*lesion_norm_fsl.sh*, *fsl_anat_alt.sh*, and *optiBET.sh*) in your path. Some familiarity with Unix command line processing is assumed.  Once everything is in your path, you will run *lesion_norm_fsl.sh* as illustrated below.

Data
******

* Images can be zipped or not (i.e., *T1w.nii* and *T1w.nii.gz* are OK).
* A structural (anatomical) T1 weighted image.
* A lesion mask with values of 1 in the lesion and 0 elsewhere.

The 3 Scripts
***************

`lesion_norm_fsl.sh <https://bitbucket.org/dpat/tools/raw/master/LIBRARY/lesion_norm_fsl.sh>`_ assumes we are in the directory with the images we need.
It expects 2 arguments:

* a T1 anatomical image
* a lesion mask (1=lesion; 0=non-lesion)

e.g.::

  lesion_norm_fsl.sh sub-001.nii.gz sub-001_LesionSmooth.nii.gz

*lesion_norm_fsl.sh* calls the two scripts described below (*fsl_anat_alt.sh* and *optiBET.sh*)

`fsl_anat_alt.sh <https://bitbucket.org/dpat/tools/raw/master/LIBRARY/fsl_anat_alt.sh>`_ is an altered version of *fsl_anat* that does several things:

* It uncomments the lesion mask handling calls that *fsl_anat* was not using.
* It calls *optiBET.sh* to improve the quality of BET, especially for lesioned brains.
* It assumes that because *optiBET* is so much better, we can use the skull-stripped brain for linear registration.

`optiBET.sh <https://bitbucket.org/dpat/tools/raw/master/LIBRARY/optiBET.sh>`_
is a simplified version of the original *optiBET*.  It calls *bin/bash* instead of *bin/sh*, and does the **skull stripping** described below using the FSL options.

.. _skull_strip:

Why Skull Strip with optiBET?
=================================

* Registration of the participant brain to the standard brain is generally better accomplished if both brains are skull stripped (but see :ref:`SPM12 <spm_lesion_norm_tpm>`).  Skull stripping is difficult, especially for individuals with large lesions.
* The *optiBET.sh* script is impressive, but you should always look at the skull stripping results and edit them if they are bad. Once you have a good skull strip (all the brain, and nothing but the brain), you are ready to go to the next step in your processing journey.


The Originals
***************

* The original **fsl_anat** is described here: `fsl_anat <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/fsl_anat>`_
* The original **optiBET** is described here: `optiBET <https://montilab.psych.ucla.edu/fmri-wiki/optibet/>`_

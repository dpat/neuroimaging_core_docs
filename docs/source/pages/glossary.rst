.. _glossary:

==========
Glossary
==========

| Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2018_08_10
| Date Updated: 2021_06_16
| Tags: Neuroimaging concepts, software, and educational resources

This is an **alphabetical index of resources and definitions related to neuroimaging**. As such it contains many links. Links and information become stale with time. Please email the maintainer if:

* You encounter a broken link
* You believe some of the information is incorrect
* You'd like to suggest additional entries

Also see the :ref:`Bookmarks <bookmarks>` page.

.. glossary::
   :sorted:

   Affine transforms
      Affine transforms are used to register images together. All parts of the image undergo the same transform (i.e., you cannot squish the image in one corner and stretch it in another, you have to stretch or squish everything.  An affine transform can be represented as a square matrix of nine numbers. See the `interactive affine matrix <https://ncase.me/matrix/>`_ to get a feel for the transformations.

   applytopup
      Applytopup is an :term:`FSL` tool used to apply distortion and movement corrections to an epi image (DWI and occasionally fMRI). Applytop depends on `topup <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/topup/TopupUsersGuide/>`_ to identify the distortions in images, and both topup and applytopup depend on the acquisition of :term:`reverse phase encode` images during the scan.
      Applytopup works like this: we have 2 images:

      * blip1 consists of two blip up (P-A) B0 volumes.  Call these volumes 1 and 2.
      * blip2 consists of two blip down (A-P) B0 volumes.  Call these volumes 3 and 4.
      * applytopup combines the first blip1 volume (1) with the first blip2 volume (3) to create a volume (1+3). Then the 2nd blip1 image (2) is combined with the second blip2 image (4) to create a volume (2+4). These two new volumes are stored in b0_hifi.nii.gz

   ASL
      Arterial Spin Labeling is an MRI modality that characterizes cerebral blood flow. Resources:

      * `ASL Analysis <https://www.mccauslandcenter.sc.edu/crnl/tools/asl>`_
      * `Andy's Brain Book: ASL <https://andysbrainbook.readthedocs.io/en/latest/ASL/ASL.html>`_

   BBR
      In FSL, `BBR <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT_BBR>`_ or *boundary-based registration* is implemented in the script `epi_reg <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT/UserGuide#epi_reg>`_, which can use :term:`field maps` to simultaneously register and distortion-correct epi images. See :term:`Difference in Echo times`.

   dcm2niix
      Chris Rorden's excellent `dcm2niix <https://github.com/rordenlab/dcm2niix/releases>`_  converts dicom to nifti. It correctly handles slice timing for multiband images and produces the bids format json sidecar files.

      Note that the naming strategy is not necessarily bids compliant.  It might be worth giving bids compliant names to your data on the scanner.  That should facilitate everything.

      If you want to perform slice time correction on multiband fMRI datasets, you should also look at `slice timing and multiband <https://www.mccauslandcenter.sc.edu/crnl/tools/stc>`_ and from the bottom of that same page, download `stcx.zip <http://www.mccauslandcenter.sc.edu/crnl/sites/sc.edu.crnl/files/attachments/stcx.zip>`_

   Difference in Echo Times
      The *difference in echo times* refers to fieldmap characteristics, and is needed for `fsl_prepare_fieldmap <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FUGUE/Guide#Making_Fieldmap_Images_for_FEAT>`_. The default value for the Siemens is 2.46. To calculate the difference in echo times, you need the TE for the first and 2nd echo: TE=4.92 (first mag); TE=7.38 (phase and 2nd mag); 1 echo (First mag map); 2 echoes (phase map and 2nd mag); 7.38-4.92=2.46. See :term:`BBR`.

   Dwell Time
      Equivalent to *echo spacing* or *time between echoes in k-space*. This is the time it took for the scanner to go from the center of one echo in the EPI sequence to the center of the next.

      The echo spacing should be listed in the parameters of the scan (e.g., 0.94 ms for my 32 direction B=1000 dwi scans). Effective echo spacing= *echo spacing* divided by *grappa* (a.k.a. acceleration, ParallelReductionFactorInPlane) factor (2 in this case) and then divided by 1000 to get units in seconds instead of ms.

      e.g., (0.94/2)/1000=0.00047

      *Effective Echo Spacing* is sometimes reported directly in the BIDS json file, e.g., in sub-001_acq-AP_dwi.json: "EffectiveEchoSpacing": 0.00047001

   Echo Spacing
      See :term:`Dwell time`.

   Effective Echo Spacing
      See :term:`Dwell time`. To run epi_reg with field maps, we need the effective echo spacing of the epi image being processed.

   Field Maps
      Field maps can be used to correct distortions in epi images (like dwi and fmri). A set of field maps contains 3 images: two magnitude images and a phase image. See :term:`BBR`. They take about 1 minute of scan time to acquire.  FSL has some nice tools for handling `Siemens field maps <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FUGUE/Guide#SIEMENS_data>`_.  You may also wish to read a more general description of `field maps <https://lcni.uoregon.edu/kb-articles/kb-0003>`_.

   FreeSurfer
      `FreeSurfer <https://surfer.nmr.mgh.harvard.edu/>`_ is a unix-based (mac or linux) neuroimaging workbench with tools for fMRI, dwi and especially structural segmentation and cortical thickness analysis. Andrew Jahn provides a useful sequence of `video lectures about FreeSurfer <https://www.youtube.com/playlist?list=PLIQIswOrUH6_DWy5mJlSfj6AWY0y9iUce>`_.

   FSL
      `FSL <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/>`_ is a unix-based (madc or linux) neuroimaging workbench with tools for fMRI, structural and especially dwi processing. FSL stands for *FMRIB Software Library*.

   FSLeyes
      `FSLeyes <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FSLeyes>`_ is a unix-based image viewer for :term:`FSL`.  It is Python-based and extensible. Andrew Jahn provides a useful sequence of `video lectures about FSLeyes <https://www.youtube.com/playlist?list=PLIQIswOrUH69qFMNg8KYkEGkvCNEwlnfT>`_.

   GIFT
      `GIFT <http://mialab.mrn.org/software/gift/index.html>`_ is a sophisticated Matlab toolbox for running ICA analysis.  A tutorial that includes links to aditional materials, is available for download `here <https://docs.google.com/document/d/1JUW_rPYYytBRoaRbzHzuCq3oPU4Y-AXdqlcwEE0otj4/export?format=pdf>`_.  These include a slightly different user manual (MAC oriented and exploring some different topics).  The manual includes links to panopto videos that walk you through the analysis.  Sample data, a 6 minute walkthrough video, presentations and sample batch files are also available.

   grappa
      GeneRalized Autocalibrating Partial Parallel Acquisition (a.k.a acceleration or ParallelReductionFactorInPlane) is a multicoil parallel imaging technique. For more information, see the excellent `MRIQuestions <http://mriquestions.com/grappaarc.html>`_ or `MR-tip <https://www.mr-tip.com/serv1.php?type=db1&dbs=grappa>`_.

   NIFTI Headers and Orientation
      Imagine you have created a mask image, but it is not in quite the right place relative to the anatomical underlay.  Where is the mask?  We can characterize it in terms of voxel coordinates OR in terms of left, right, anterior posterior etc. This is *voxel space* and *world space* respectively. Just because a display tool (like FSLeyes) allows you to nudge the position of the mask, does not guarantee that the underlying voxel positions are changed. This is very confusing: when you look at the mask in the display tool, it is displayed in the new nudged position, but when you perform image math operations, it is as if the position had never been nudged.

      In FSLeyes 0.27.3 you need to save the affine transformation corresponding to your nudge and then apply it to the mask with FLIRT if you want to actually change the baked in position of the mask.  This is similar to coregistration and reslicing which actually change the baked-in positions of the intensities at each voxel. Once you have changed the actual values of the voxels, your mask will not only display in the nudged position but will actually BE in the nudged position and thus useful for all image math tools. See also `FSL's Orientation Explained <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Orientation%20Explained>`_.

      There is another wrinkle.  There has to be some way to relate voxel space to world space.  The NIFTI header contains two (not one, sigh) ways to encode this relationship: the sform and qform. ``S`` seems to stand for *standard* whereas ``Q`` stands for quaternion.  This has led to two different camps: the sform aficionados (SPM, FSLeyes, Mango, MRIcroGL) and the qform aficionados (ANTS, ITK-SNAP, MRtrix). The sform aficionados record any change in the world-to-voxel relationship in the sform matrix; however the qform aficionados record it in the qform.  Then each camp has to make a choice about what to do with the other camp's matrix:

      #. Leave the other matrix alone, allowing the two matrices to be different.
      #. Set the other matrix the same as your matrix, so any software will see the change.
      #. Set the other matrix to all zeros (and assume that *smart* software will look at the other matrix when their favorite matrix has been set to zeros.

      As different versions of a particular piece of software have arrived, developers have sometimes switched strategy.  For example, SPM used to update only the sform (strategy 1) but more recently has adopted strategy 2. ITK-SNAP uses strategy 3. MRtrix generally uses the qform now, but you can set a preference to use the sform instead (which used to be the default). If the sform and qform differ, MRtrix will warn you.


   Phase Encode Direction
      If you are using :term:`field maps`, epi_reg requires a phase encode direction (--pedir) for the primary dwi image. Unfortunately, the  BIDS JSON files use i,j,k and :term:`FSL` uses x,y,z to record this value. "PhaseEncodingDirection": "j-" in the json file corresponds to -y in FSL's terminology (-y=AP).

   Q-form
      A part of the NIFTI header. See :term:`NIFTI Headers and Orientation`

   Q-space Sampling
      For DWI, the distribution of gradients used to sample the diffusion space. An interactive tool for exploring q-space sampling (and creating your own gradient tables): `q-space <http://www.emmanuelcaruyer.com/q-space-sampling.php>`_

   Reverse Phase Encode
      DWI data are acquired in one direction (e.g., A->P).  Because the echo time for DWI images is so long, this means there is tremendous distortion.  If you look at A->P images in axial view, you'll see stretchy eyeballs.  A second image (1-2 B0s) in the opposite phase-encode (P->A) direction takes ~45 seconds to acquire and can be used to help correct distortion in the first image.  This second image experiences tremendous distortion in the opposite direction of the original A->P images. If you look at the P->A image in axial view, you'll see it has squishy eyeballs.

      * The default Siemens A-P: stretchy eyeballs, (negative phase encode, blip down -1)
      * The reverse phase encode B0 P-A: squishy eyeballs, (positive phase encode, blip_up 1)

      * In :term:`FSL` we can use `topup <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/topup/TopupUsersGuide/>`_ to calculate the distortion parameters based on both images. We can then use `applytopup <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/topup/ApplyTopupUsersGuide>`_ to apply these parameters to one of the images. This can be helpful for creating a corrected B0 image for skull stripping.
      * N.B. My experiments suggest it is better to correct the A-P data (less noise, fewer spikes and holes in weird places, better eyeball reconstruction).
      * Finally, we feed the topup results into `eddy <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/eddy/UsersGuide>`_ for correcting eddy currents and movement.
      * The following text files contain parameters needed for processing:
        * acqp_A-P
        * acqparams.txt
        * index_A-P.txt
      * The `acqp file <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/eddy/UsersGuide>`_ is used by eddy and topup. This is a text-file describing the acquisition parameters for the dwi volumes. In eddy this parameter file is passed with the flag --imain. In topup the parameter is passed with the flag --datain.
      * The value is repeated 32 times for the 32 volumes.  The -1 indicated the phase encode direction for y in A-P.  The last value is :term:`Total Readout Time`.
      * acqparams.txt:  Used for topup.  Specifies the values for the 4 blip_down and blip_up B0 volumes
      * index_A-P.txt: Used by eddy to index all the values in acqp_A-P.txt (all 1's)

   S-form
      A part of the NIFTI header. See :term:`NIFTI Headers and Orientation`

   Topup
      `Topup <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/topup/TopupUsersGuide/>`_ is an :term:`FSL` tool primarily used together with `applytopup <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/topup/ApplyTopupUsersGuide>`_  and `eddy <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/eddy/UsersGuide>`_ to correct distortion in DWI (and occasionally fMRI) images. Topup relies on :term:`reverse phase encode` images.

   Total Readout Time
      Total readout time is the echo spacing (:term:`dwell time`) * the number of phase encoding steps. Total readout time applies to the epi sequence (usually dwi) and appears in the acqp and acqparams files used by :term:`FSL`'s topup. If you use grappa, divide the number of phase encoding steps by the grappa factor. For example, if echo time=0.94 ms (but we want it in seconds, so 0.00094 sec), phase encoding steps=128, and grappa=2 then total readout time is (128/2)*.00094 sec =0.06016 sec

   Wavelets
      `An accessible introduction to wavelets <https://georgemdallas.wordpress.com/2014/05/14/wavelets-4-dummies-signal-processing-fourier-transforms-and-heisenberg/>`_

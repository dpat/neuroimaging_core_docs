.. _moreunix:

============================================
More Unix: Install Neuroimaging Tools
============================================

| Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2019_08_21
| Date Updated: 2019_08_21
| Tags: UNIX

.. toctree::
   :hidden:
   :maxdepth: 3

We want to pull together things you have learned to make you more independent.
It is one thing to run commands on a system someone else has set up for you. But it requires a bit more understanding to set up things yourself.

Install FSL
****************

I am going to illustrate with FSL scripts, because I know that best and there are a number of scripts available on this site that rely on FSL.  This means you will want to `install FSL <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation>`_. Installing FSL can be a big job.  Be prepared to follow the instructions on the FSL site carefully.  It is possible to have problems even if you follow the instructions (e.g., ``imcp``, ``immv`` and several related commands sometimes fail to install correctly).  Join the forum, try the install, take some breaks. If you can't make it work, send a detailed description of the problem to the FSL list (your operating system, what you tried, what error messages you get). Be persistent, keep notes, and forgive yourself your errors.  Once FSL is installed, you can continue with the steps described below.

-----

Set up a Tools Directory
*****************************

In this section, you will download some scripts and make sure they are executable and in your path.

**Step 1**:  Go to your home directory and create a subdirectory called tools::

  cd ~
  mkdir tools

**Step 2**: Go to the `Prepare T1w Images <https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/image_processing_tips.html#prepare-t1w-image>`_ section of these pages.  Click the links for each of the three scripts (``prep_T1w.sh``, ``fsl_anat_alt.sh`` and ``optiBET.sh``), and download each one of them into your tools directory.

**Step 3**: Make sure the permissions on those scripts will allow anyone to execute them::

  cd tools
  chmod a+x *.sh

**Step 4**: Find out which shell you are running (e.g., bash, tcsh, csh, zsh etc). You are probably running bash (since that is the default shell on most machines).  You can check like this::

  echo $SHELL

**Step 5**: Find or create your shell configuration file. There should be a shell configuration file in your home directory (``.cshrc`` for csh, ``.tcshrc`` for tsh, ``.bashrc`` for bash).  The configuration file will be hidden (i.e., it'll start with a ``.``).  You may find you have configuration files for several different shells, or none at all.

First make sure you are in your home directory (not in tools). Then display all hidden files to see what configuration files you have::

  cd ~
  ls -la

If you don't have any configuration files, then all of your settings come from a shared configuration file in the ``/etc`` system area.  Don't worry.  You can simply create your own, e.g.,::

  touch .bashrc

You can find minimal configuration file examples on the internet, e.g., `minimal .bashrc and .bash_profile <https://gist.github.com/jlcorra/8f7f6bd02ff0af4b8a22>`_

.. Warning:: Configuration files from different shells are not interchangeable.  The way the paths and aliases are created can differ considerably.  That said, ``bash`` and ``zsh`` are very similar to each other and should be able to use the same configuration statements.  ``tcsh`` and ``csh`` are also similar to one another and pretty interchangeable.  However, tcsh and csh are very different from bash and zsh.

**Step 6**: View your configuration file.  You don't need an editor to view your configuration file.  You can use ``cat``, e.g.,::

  cat .bashrc

**Step 7**: If there is something in your configuration file already, you don't want to mess it up.  Make a copy right now, e.g.,::

  cp .bashrc bashrc_orig

**Step 8**: Edit your configuration file.  You will want to change and test your configuration file and you need an editor to do that.  There is always an editor called ``vi`` available, but it is really old-fashioned and difficult to use.  You should learn to work with it someday, but not now.  On the mac, you should have ``textedit``, and on Ubuntu you should have ``gedit`` by default.  You may want to install an editor like `atom <https://atom.io/>`_. Once you have found or installed your editor, you can open the configuration file of choice with it, e.g.::

  gedit .bashrc

**Step 9**: Add a line to your configuration file to ensure the operating system will look for executable files in your tools directory.  This is an example of a bash path (You can look for examples online for other shells).::

  PATH=$PATH:~/tools

This says: the path shall contain everything previously defined for the path AND the directory ``tools`` in my home area.

Save your changes.

**Step 10**: Did you break it?  From your home directory, source your configuration file, e.g.,::

  source .bashrc

If nothing happens, you did it right! If there are errors, read the messages!  As soon as the system finds an error in your configuration file, it stops trying to understand any subsequent statements.  If you have a backup, you can source that to see if it had the same problem (or if it was indeed your fault), e.g.,::

  source bashrc_orig

**Step 11**: Can the operating system find your scripts?::

  which fsl_anat_alt.sh

We hope the operating system will find the script you put in ``~/tools``.  If so, you did it! If the operating system says ``fsl_anat_alt.sh not found``, then check these things:

* Are the scripts executable?
* Did you save the changes to your configuration file?
* Did you source the configuration file?

.. Warning:: If you have several shell windows open (e.g., several bash windows, for example), sourcing your ``.bashrc`` in one of your windows only affects that window!  If you open a new bash window, it will automatically source the ``.bashrc`` when it starts up.  But, windows that were already open do not know about changes in the ``.bashrc`` until you force them to by typing ``source .bashrc``.

If you got through that, congratulations!  You have learned a really important skill for taking control of your computing environment.

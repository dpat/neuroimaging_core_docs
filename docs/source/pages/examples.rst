===========
Main Header
===========

Parts
*****

Section
=======

Subsection
----------

Subsubsection
^^^^^^^^^^^^^

It seems that the number of header types you choose for a page affects how they are displayed (they seem to be aware of each other).  The underlining for headers must be at least as long as the title, but could be longer.
There appears to be some variation in these header markups.
It also seems that you CANNOT skip a level in the hierarchy (e.g., you cannot go from ****** to -----.).

To break longer text up into sections, you use section headers. These are a single line of text (one or more words) with adornment: an underline alone, or an underline and an overline together, in dashes "-----", equals "======", tildes "~~~~~~" or any of the non-alphanumeric characters = - ` : ' " ~ ^ _ * + # < > that you feel comfortable with. An underline-only adornment is distinct from an overline-and-underline adornment using the same character. The underline/overline must be at least as long as the title text. Be consistent, since all sections marked with the same adornment style are deemed to be at the same level.

Transition Marker:
You can insert a dividing line

A transition marker is a horizontal line
of 4 or more repeated punctuation
characters.

------------

A transition should not begin or end a
section or document, nor should two
transitions be immediately adjacent.

----

This is something new July 1, 2019 9:11pm

Extension Mermaid for Sequence diagrams and flowcharts
*************************************************************

.. mermaid::

   sequenceDiagram
      participant Alice
      participant Bob
      Alice->John: Hello John, how are you?
      loop Healthcheck
          John->John: Fight against hypochondria
      end
      Note right of John: Rational thoughts <br/>prevail...
      John-->Alice: Great!
      John->Bob: How about you?
      Bob-->John: Jolly good!


-----

This next one is a flowchart. TD means Top-Down (semicolons are no longer necessary)

.. mermaid::

  graph TD
    title[<b><u>My Title</b></u>]
    title-->B
    title-->C
    B-->D
    C-->D

Testing
==========

.. mermaid::

  graph TD
    c1-->a2
    subgraph one
    a1-->a2
    end
    subgraph two
    b1-->b2
    end
    subgraph three
    c1-->c2
    end

Line Breaks with a Pipe
***************************

| Maintainer: Dianne Patterson dkp @ email.arizona.edu
| Date Created: 2018_06_14
| Date Updated: 2018_06_22
| Tags:


Emphasis
********

| Italics in single asterisks like this: *italics*
| Bold in double astericks like this: **bold**
| Code samples in double backtics, like this:  ``code sample``

Set off in a box:

For example, the first line of my nrm_job.m is::

    matlabbatch{1}.spm.spatial.preproc.channel.vols=...
    {'/Volumes/Main/Working/LesionMapping/DataC8/sub-001/T1w.nii,1'};

* We need something more generic.


Table of contents tree (sphinx)
**********************************

.. toctree::
   :hidden:
   :maxdepth: 2

   /pages/lesion

Making an autonumbered list
***************************************************************

Use # leave blank lines around it:

#. Creating the initial lesion mask
#. Using the mask properly in the normalization process
#. Running statistical tests to evaluate the relationship between the lesion (volume and location) and any associated behavioral measures.

Nested List
*************

* Run the batch module by clicking the green go button (upper left on batch editor).
* Segmentation produces

  * files prefixed with **c** for each tissue type and the skull.
  * a *seg8.mat* is produced containing segmentation parameters.
  * a deformation field with a **y** prepended. (This is a warp field rather than a recognziable brain image.)
  * If you elected to create an Inverse deformation as well, that would be prefixed with **iy**.

* A new lesion image with **w** prefixed to the name now appears.  This is the lesion mask warped into standard space. N.B. The mask is not binary

Special characters
******************
Copy the unicode characters.  Here is a page of arrows:
`unicode arrows <https://unicode-table.com/en/sets/arrows-symbols/>`_

→  ➜  ➔

Make them bigger by making them headers (don't forget to leave a blank line):

→  ➜  ➔
********


Linking
*******

Inline Links to external resources
====================================

`ant_reg2.sh <https://bitbucket.org/dpat/tools/src/master/LIBRARY/ant_reg2.sh>`_
blah blah

Links to Raw Code
--------------------

To link to the raw file (which is suitable for download), substitute master for the commit hash: https://bitbucket.org/dpat/tools/raw/d12887a1fb59069b20a3f3756ee69a2a9e8176e4/HPC/runfmriprep.sh  becomes https://bitbucket.org/dpat/tools/raw/master/HPC/runfmriprep.sh

Named Links
===========

URLs that are referenced from multiple locations in the document can be defined
once and referenced by name. In the following example, *cheatsheet*
refers to a named link to an external URL, which is defined below (but not visible)
on this examples page:

This an indirect link to an online RST `cheetsheet`_

.. _cheetsheet: https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst

Linking to a download
=====================

Downloads must appear in a directory under source.  You can use a relative link (relative to current page) OR as in this example, start with / and provide the link path from the source dir.

:download:`convertall.py </data/convertall.py>`

Link to glossary
=================

Example of linking to a given term on the glossary page:
:term:`FSL` is a terrific toolset. :term:`fsl` (in lower case) also works as a glossary link.

You can link to a term in the glossary while showing different text in the topic by
including the term in angle brackets. For example, :term:`FMRIB Software Library<FSL>`
will display FMRIB software library to the user but link to the glossary item FSL.

Popup abbreviation
====================

This :abbr:`SDP (Software Development Plan)` blah blah.  This provides a tooltip in HTML.


Images
================

FSL is sometimes pronounced *fossil* as is hinted at by their logo (how to insert an image):

.. image:: /pictures/fsl-logo.png

You can add more image info: The images/biohazard.png part indicates the filename of the
image you wish to appear in the document. There's no restriction placed on the image
(format, size etc). If the image is to appear in HTML and you wish to supply additional
information, you may:

.. image:: /pictures/fsl-logo.png
   :height: 100
   :width: 200
   :scale: 50
   :alt: fossil trilobyte

Here is a figure with a caption: Cannot use initial number in figure caption, though you can in the legend.  Cannot define a figure with a substituion (which you can do with images). I had trouble every time I tried to enter ``:alt:`` or any other modifiers...even straight from an example, it just breaks.

.. figure:: /pictures/TMS_Interface.png

      Five important areas: 1. Control area; 2. Display; 3. Toolbar; 4. Menu; 5. Status bar


Link to sections
=================

This link points to a section in the spm.rst page:
see :ref:`Batch and Scripting <batch-scripts-spm>`

This link points to the example below (not exciting):
see :ref:`blah blah <example>`

You must ensure that the section is labeled, something like this: ``.. _example:``

.. _example:

Links can work across pages, too. This :ref:`glossary` will take you
to the Glossary page, which is labeled ``.. _glossary:``


Example Section
===============

This section is a target for a section link named 'example'.

sidebar
*************

You have to fool with the size of the image so they end up fitting next to each other.
The sidebar is on the right of the material below it (in this case, an image)

.. sidebar:: MEP (Muscle Evoked Potentials)

   MEP (Muscle Evoked Potentials) is used for identifying the minimum stimulation threshold needed for activation. This is done by stimulating the hand knob in the brain and ensuring that the stimulation can successfully activate a finger twitch (3/6 times).  Whatever that level of stimulation is, we then calculate 70% of it to use for activation in subsequent stimulation procedures. The MEP system consists of a computer (black Dell), a data acquisition device (black) and amplifier (white) on a cart.

.. image:: /pictures/TMS_hardware_MEP.png
   :width: 24%


Insert a table
***************

.. csv-table:: Frozen Delights!
 :header: "Treat", "Quantity", "Description"
 :widths: 15, 10, 30
 :align: left

 "Albatross", 2.99, "On a stick!"
 "Crunchy Frog", 1.49, "If we took the bones out, it wouldn't be
 crunchy, now would it?"
 "Gannet Ripple", 1.99, "On a stick!"


another table type, the list table, has exactly the same look.

.. list-table:: Frozen Delights!
   :widths: 15 10 30
   :header-rows: 1

   * - Treat
     - Quantity
     - Description
   * - Albatross
     - 2.99
     - On a stick!
   * - Crunchy Frog
     - 1.49
     - If we took the bones out, it wouldn't be
       crunchy, now would it?
   * - Gannet Ripple
     - 1.99
     - On a stick!

Comments
********
We can make a comment and add to it if we indent

.. this is a comment
   and this is more

Nothing was rendered there, even in the html build

Warnings etc.

.. Warning:: I see no evidence that the original warp field is weighted by the lesion mask.  I do not understand this.

.. Note:: This is a note

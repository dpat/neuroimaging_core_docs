.. _lesions:

===============
Stroke Lesions
===============

| Maintainer: Dianne Patterson Ph.D. dkp @ arizona.edu
| Date Created: 2018_06_14
| Date Updated: 2019_07_26
| Tags: anat, lesion, stroke, segmentation, CT, header editing, registration, normalization

The focus of these pages is large lesions, such a stroke lesions. These large lesions present problems not encountered with smaller punctate lesions (e.g., MS lesions):

#. :ref:`Creating the Lesion Mask <creatingthelesionmask>`: Large stroke-related lesions are difficult to characterize. Manual methods are laborious and the inter-rater reliability hovers around 80%. Automated routines perform poorly. We explore semi-automated pipelines that can help with both the speed and consistency of drawing lesion masks.
#. :ref:`Working with Clinical Data <clinical_data>`: Although we use a large homogenous dataset to test normalization techniques, real stroke studies may have more heterogenous data. For example, stroke patients who cannot have MRI scans may sometimes have CT scans. Working with the available imaging data presents unique challenges.
#. :ref:`Lesion Normalization <lesion_normalization>`: A large area of abnormal tissue can adversely affect how the image is warped into standard space. This can result in lesion shrinkage, as discussed in `Andersen, S. M., Rapcsak, S. Z., & Beeson, P. (2010). Cost function masking during normalization of brains with focal lesions: Still a necessity? NeuroImage. <https://www.ncbi.nlm.nih.gov/pubmed/20542122>`_. Here I explore normalization in a homogenous dataset comparing the results of using different tools and parameters.
#. **Statistical Analysis of Lesions**: Both volume and location of the lesion should be considered in statistical analyses (otherwise there is the danger of simply discovering that big lesions damage more behaviors).


.. toctree::
   :hidden:
   :maxdepth: 3

   /pages/lesion_create-mask
   /pages/clinical_scans
   /pages/lesion_normalization

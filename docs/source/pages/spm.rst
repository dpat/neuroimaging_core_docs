====
SPM
====

| Maintainer: Dianne Patterson dkp @ arizona.edu
| Date Created: 2018_06_14
| Date Updated: 2018_11_17
| Tags: Neuroimaging software, SPM12, Matlab
| OS: Mac, Linux or Windows with Matlab (no toolboxes necessary)

.. toctree::
   :hidden:
   :maxdepth: 3

   /pages/spm_anat_normalization-lesion
   /pages/spm_anat_normalization-lesion-tpm
   /pages/spm_anat_normalization-lesion-old
   /pages/spm_anat_normalization-rev

.. _batch-scripts-spm:

Among his other tutorials, Rajeed Raizada provides some excellent interactive Matlab tutorials for understanding how SPM analysis works: `Standard fMRI Analysis <http://www2.bcs.rochester.edu/sites/raizada/fmri-matlab.html>`_
See especially hrf_tutorial.m math_of_convolution.m and design_matrix_tutorial.m


Batch and Scripting
=======================

* Use the SPM GUI to create a batch script that you wish to apply to other subjects.
* See :ref:`SPM Lesion Normalization <spm_lesion_norm>` for an example of creating a batch job
* Insure that the batch job runs properly.
* If the file names are the same for all subjects, this will make your life easier. If not you can link some simplified names to your subject-specific names. Again, see :ref:`SPM Lesion Normalization <spm_lesion_norm>`.
* In the batch editor, choose *File ➜ Save Batch and Script*, and type in a name (e.g., nrm1). This will save two m files, e.g., *nrm1.m* and *nrm1_job.m*. Put these in a directory in your Matlab path.
* If you want to load the batch up in the batch editor again, start the editor and load the *job.m* file
* Look at the job file first. You see repeated calls to matlabbatch.  Each call is followed by a number, e.g., {1} for the first batch module you set up, {2} for the next one, etc.
* There are several spots where the path to your data is specified.  We'll want to change these.  For example, the first line of my nrm_job.m is::

    matlabbatch{1}.spm.spatial.preproc.channel.vols={'/Volumes/Main/Working/LesionMapping/DataC8/sub-001/T1w.nii,1'};`

* We need something more generic than */Volumes/Main/Working/LesionMapping/DataC8/sub-001/T1w.nii,1*. So I add this line at the top of the script to define a variable for the anatomical image name::

    T1='T1w.nii'`

* Now I modify the line in question to look like this (substituing my generic variable for the full path to the file for sub-001::

    matlabbatch{1}.spm.spatial.preproc.channel.vols = {T1};

* Define any other image files using the same idea, and substitute your new image names with these variables throughout the script. If you list multiple file variables, then separate them by commas, and insure they appear in a column, like this::

    {
      T1,
      lesion,
      T1_brain
     }

* Now we are going to change the other file (e.g., nrm.m) completely.  Our goal is to define the data directory path and the subject folder names (This assumes all of your subject data is in subject directories in a single folder).  Then we will loop through the subjects, cding to each subject directory, running our batch job, and then going back up to the dataDir before starting on the next subject::

    % This is a comment: clear any existing variables
    clc; clear all
    % Insure SPM is ready to go
    spm('defaults', 'FMRI');
    % Define the data directory path containing your subject folders
    dataDir = '/Volumes/Main/Working/LesionMapping/DataC8/Test/';
    % Define a list of subject directories (probably longer than this)
    subjList={'sub-030','sub-031','sub-033'}
    % for each subject in subjList, put the subject in a variable called "subj"
    for subj = subjList
        % display the subject (not necessary, but helpful)
        disp(strcat('Subject:   ',subj));
        % clear matlabbatch variables (we don't want any leftovers contaminating this run)
        clear matlabbatch;
        % cd into the subject directory.
        cd(char(subj));
        % call the job nrm1_job.m
        spm_jobman('run','nrm1_job.m')
        % When finished, go back up to the dataDir
        cd(dataDir)
    end;


* That's it.  Test your script by typing the name of the file we just modified (using a small subject list)::

  >>nrm1

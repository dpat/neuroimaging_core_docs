U of A: Neuroimaging Core Documentation
=============================================
| Maintainer: Dianne Patterson dkp @ email.arizona.edu
| Date Created: 2018_06_14
| Date Updated: 2019_11_14

These pages provide documentation for the neuroimaging core at the University of Arizona. The focus is on approaches used and/or developed at the University of Arizona.

At the top of each page is a record of the maintainer, date created and date updated to help the user identify information that is likely to be stale, and notify the maintainer.

Pages that describe processing using particular neuroimaging software should include software version and OS version information.

**Want PDFS?** Read the Docs has the ability to output documentation in other formats: pdf and epub.  On the bottom left it says ``Read the Docs v:latest``. Under ``Downloads``, choose one of the ``latest`` options (e.g. PDF).  The PDFs have much nicer formatting than if you simply "save as" pdf in your browser. The only downside is that the pdf includes everything on the website, so you'll want to use Preview or Adobe Acrobat or something similar to extract the portion of the documentation you want. Of course, you could just print the range of pages you want as hard copies.

.. toctree::
   :caption: Table of Contents
   :name: mastertoc
   :hidden:
   :maxdepth: 3

   /pages/atlases
   /pages/bids
   /pages/bookmarks
   /pages/choropleths
   /pages/glossary
   /pages/image_processing_tips
   /pages/resting_state
   /pages/lesion
   /pages/tms

.. toctree::
   :caption: Neuroimaging Software
   :hidden:
   :maxdepth: 3

   /pages/ants_anat_normalization-lesion
   /pages/fsl
   /pages/itk-snap
   /pages/mango
   /pages/spm

.. toctree::
   :caption: Unix
   :hidden:
   :maxdepth: 3

   /pages/unix
   /pages/more_unix
   /pages/hpc
   /pages/singularity_hpc
   /pages/cyverse

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
